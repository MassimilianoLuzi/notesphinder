close all;
clear all;
clc

%addpath(genpath('C:\Users\Massimiliano\Google Drive\UniversitÓ\Dottorato\AMT\Nuovi Test\Midi compare\matlab-midi-master'));

addpath('C:\Users\Massimiliano\Google Drive\UniversitÓ\Dottorato\AMT\Nuovi Test\Midi compare\Vincent');

nameInstrument = {'Steinway Grandpiano';
                  'Yamaha C2';
                  'Disklavier'};

% Set file path of out midi files           
txtPath = {'Steinway Grandpiano'; 
               'Yamaha C2';
               'Disklavier'};    

txtFileNames = {dir(fullfile(txtPath{1}, '*.txt'));
          dir(fullfile(txtPath{2}, '*.txt'));
          dir(fullfile(txtPath{3}, '*.txt'))};      
  
for i=1:3
    numFiles = length(txtFileNames{i});
    
    for n = 1:numFiles   
        currentFilePath = fullfile(txtPath{i}, txtFileNames{i}(n).name);
        [~, txtFileName, ~] = fileparts(currentFilePath);
        fprintf('Instrument: %s \n \t \t out: %s \n', nameInstrument{i}, txtFileName); 
        
        vincentOutFile = textread(currentFilePath);

        midiMatrix = zeros(size(vincentOutFile,1), 6);
        midiMatrix(:,1) = 1;
        midiMatrix(:,2) = 1;
        noteFreq = 32.7*2.^((0:71)/12);
        for m=1:size(midiMatrix,1)
            [~, index] = min(abs(vincentOutFile(m,3)-noteFreq));
            midiMatrix(m,3) = index - 1 + 24;
        end
        midiMatrix(:,4) = 100;
        midiMatrix(:,5) = vincentOutFile(:,1);
        midiMatrix(:,6) = vincentOutFile(:,2);
        midi = matrix2midi(midiMatrix, 50);
        fileName = strcat(txtFileName,'.mid');
        midiPath = fullfile(txtPath{i}, fileName);
        writemidi(midi, midiPath);
    end
end

