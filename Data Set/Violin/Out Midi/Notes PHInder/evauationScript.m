close all;
clear all;
clc

addpath(genpath('C:\Users\Massimiliano\Google Drive\UniversitÓ\Dottorato\AMT\Nuovi Test\Midi compare\matlab-midi-master'));

addpath('C:\Users\Massimiliano\Google Drive\UniversitÓ\Dottorato\AMT\Nuovi Test\Midi compare');

addpath(genpath('C:\Users\mgilm\Google Drive\UniversitÓ\Dottorato\AMT\Nuovi Test\Midi compare\matlab-midi-master'));

addpath('C:\Users\mgilm\Google Drive\UniversitÓ\Dottorato\AMT\Nuovi Test\Midi compare');

% Set file path of output files
sourceMidiPath = 'C:\Users\Massimiliano\Google Drive\UniversitÓ\Dottorato\AMT\Nuovi Test\Violino\Ref Midi';   
sourceMidiPath = 'C:\Users\mgilm\Google Drive\UniversitÓ\Dottorato\AMT\Nuovi Test\Violino\Ref Midi';   
sourceMidiFileNames = dir(fullfile(sourceMidiPath, '*.mid'));
% Set file path of out midi files           
outMidiPath = 'Mid';     

outMidiFileNames = dir(fullfile(outMidiPath, '*.mid'));
      
results = struct(...
    'InstrumentName', [], ...
    'FrameResults',   zeros(11, 5),...
    'OnsetResults',   zeros(11, 4));
      

    numSourceMidi = length(sourceMidiFileNames);
    numOutMidi = length(outMidiFileNames);
    if numSourceMidi ~= 11||numOutMidi ~= 11||numSourceMidi ~= numOutMidi
        error('The number of midi files is less than 11');
    end
    
    results.InstrumentName = 'Vincent Violin';
    for n = 1:numOutMidi   
        sourcePath = fullfile(sourceMidiPath, sourceMidiFileNames(n).name);
        outPath = fullfile(outMidiPath, outMidiFileNames(n).name);
        
        [~, sourceName, ~] = fileparts(sourcePath);
        [~, outName, ~] = fileparts(outPath);
        fprintf('source: %s \n \t out: %s \n',sourceName, outName); 
        
        sourceMidi = readmidi(sourcePath);
        outMidi = readmidi(outPath);
        
        [frameAcc, Esub, Efn, Efp, Etot] = frameLevelCompare(sourceMidi, outMidi);
        results.FrameResults(n,1) = frameAcc;
        results.FrameResults(n,2) = Esub;
        results.FrameResults(n,3) = Efn;
        results.FrameResults(n,4) = Efp;
        results.FrameResults(n,5) = Etot;
        
        [onsetAcc, Precision, Recall, F_measure] = onsetLevelCompare(sourceMidi, outMidi);
        results.OnsetResults(n,1) = onsetAcc;
        results.OnsetResults(n,2) = Precision;
        results.OnsetResults(n,3) = Recall;
        results.OnsetResults(n,4) = F_measure; 
    end
    table = {'Frame Level', 'frameAcc', 'Esub', 'Efn', 'Efp', 'Etot'};
    xlswrite('results', table, 'Vincent', 'A1');
    xlswrite('results', results.FrameResults, 'Vincent', 'B2');
    xlswrite('results', {'AVG'}, 'Vincent', 'A12');
    xlswrite('results', mean(results.FrameResults,1), 'Vincent', 'B12');
    
    table = {'Onset Level', 'onsetAcc', 'Precision', 'Recall', 'F_measure'};
    xlswrite('results', table, 'Vincent', 'H1');
    xlswrite('results', results.OnsetResults, 'Vincent', 'I2');
    xlswrite('results', {'AVG'}, 'Vincent', 'H12');
    xlswrite('results', mean(results.OnsetResults,1), 'Vincent', 'I12');

save('results.mat');

      