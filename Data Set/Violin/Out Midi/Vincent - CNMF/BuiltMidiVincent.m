close all;
clear all;
clc

%addpath(genpath('C:\Users\Massimiliano\Google Drive\UniversitÓ\Dottorato\AMT\Nuovi Test\Midi compare\matlab-midi-master'));

addpath('C:\Users\Massimiliano\Google Drive\UniversitÓ\Dottorato\AMT\Nuovi Test\Midi compare\Vincent');
addpath('C:\Users\mgilm\Google Drive\UniversitÓ\Dottorato\AMT\Nuovi Test\Midi compare\Vincent');


txtFileNames = dir(fullfile('*.txt'));      
  

    numFiles = length(txtFileNames);
    
    for n = 1:numFiles   
        fprintf('out: %s \n', txtFileNames(n).name); 
        
        vincentOutFile = textread(txtFileNames(n).name);

        midiMatrix = zeros(size(vincentOutFile,1), 6);
        midiMatrix(:,1) = 1;
        midiMatrix(:,2) = 1;
        noteFreq = 32.7*2.^((0:71)/12);
        for m=1:size(midiMatrix,1)
            [~, index] = min(abs(vincentOutFile(m,3)-noteFreq));
            midiMatrix(m,3) = index - 1 + 24;
        end
        midiMatrix(:,4) = 100;
        midiMatrix(:,5) = vincentOutFile(:,1);
        midiMatrix(:,6) = vincentOutFile(:,2);
        midi = matrix2midi(midiMatrix, 50);
        fileName = strcat(txtFileNames(n).name,'.mid');
        writemidi(midi, fileName);
    end


