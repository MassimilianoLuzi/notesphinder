% Copyright (c) 2015, 2016 Massimiliano Luzi, Mario Antonelli and Antonello Rizzi
% University of Rome "La Sapienza"
%
% massimiliano.luzi@uniroma1.it
% mario.antonelli@gmail.com 
% antonello.rizzi@uniroma1.it 
%
% This file is part of NotesPHInder.
%
% NotesPHInder is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
% 
% NotesPHInder is distributed in the hope that it will be useful. 
% IT IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
% INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
% PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
% HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
% CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
% OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 
% See the GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with NotesPHInder.If not, see<http://www.gnu.org/licenses/>.

function activityMatrix = GetActivityMatrix(Notes, ts)
%
% Inputs:
%  Notes: A 'notes' matrix as returned from midiInfo.m
%  ts:    (optional) time step of one 'pixel' in seconds (default 0.01)
%
% Outputs:
%  activityMatrix: matrix with ones in frame with onset event
%
%

if nargin < 2
  ts = 0.01;
end

Nnotes = 72;

onsetTime = round(Notes(:,5)/ts)+1; % start tics
offsetTime = round(Notes(:,6)/ts)+1; % end tics

%Notes(:,3) = Notes(:,3) + (Notes(:,3)==0); % correct zeros in the tone
activityMatrix = zeros(Nnotes, max(offsetTime));

for i=1:size(Notes, 1) 
    if (Notes(i, 3) - 24 + 1 <= 72)
        activityMatrix(Notes(i,3) - 24 + 1, onsetTime(i):offsetTime(i)) = 1;
    end
end