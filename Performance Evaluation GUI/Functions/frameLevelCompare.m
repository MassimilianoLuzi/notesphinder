% Copyright (c) 2015, 2016 Massimiliano Luzi, Mario Antonelli and Antonello Rizzi
% University of Rome "La Sapienza"
%
% massimiliano.luzi@uniroma1.it
% mario.antonelli@gmail.com 
% antonello.rizzi@uniroma1.it 
%
% This file is part of NotesPHInder.
%
% NotesPHInder is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
% 
% NotesPHInder is distributed in the hope that it will be useful. 
% IT IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
% INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
% PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
% HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
% CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
% OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 
% See the GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with NotesPHInder.If not, see<http://www.gnu.org/licenses/>.

function [Dixon, Esub, Efn, Efp, Etot, sourceActivityMatrix, outActivityMatrix] = frameLevelCompare(midiSource, midiOut)
%% Summary
% Return the Dixon formule to compute performance in Automatic
% Music Trascription and the number of notes correctly transcribed,

%% PARAMETERS:
% midiSource: file name of Midi Source.
% midiTranscribed: file name of Midi Output.

%% OUTPUTS:
% output: Dixon out.
% correctlyTranscribed: #notes correctly transcribed.
% falseNegative: #notes not transcribed.
% falsePositive: #notes wrongly transcribed
%%
%addpath(genpath('matlab-midi-master'));

% extract midi info.
infoMidiSource = midiInfo(midiSource);
infoMidiOut = midiInfo(midiOut);

% set frame lenght in seconds
frameLength = 0.01;

% Get Activity Matrix using frameLength.
% Activity Matrix has 1 where the note is played, 0 otherwise.
sourceActivityMatrix = GetActivityMatrix(infoMidiSource, frameLength);
outActivityMatrix = GetActivityMatrix(infoMidiOut, frameLength);

% Get onset Matrixes using frame of frameLength.
% Onset Matrix has Rows for each notes, and Columns rapresents frame.
% '1' value in (i,j) position rapresents the presence of onset on note 'i'
% at 'j' frame.
sourceOnsetMatrix = GetOnsetMatrix(infoMidiSource, frameLength);
outOnsetMatrix = GetOnsetMatrix(infoMidiOut, frameLength);

% Inizialize delta varaible used for compare onset instants 
% and realign midis
deltaSrc2Out = zeros(size(infoMidiSource,1), 1);
eventNumberSrc = 1;

deltaOut2Src = zeros(size(infoMidiOut,1), 1);
eventNumberOut = 1;

for note = 1:72
   % for each notes extract the frames in witch there are onsets.
   onsetIndexSrc = find(sourceOnsetMatrix(note, :) == 1);
   onsetIndexOut = find(outOnsetMatrix(note, :) == 1);
   
   % Extract number of occurance of the note-ma note both in
   % Src Midi and Out Midi
   nSrcNotes = size(onsetIndexSrc, 2);
   nOutNotes = size(onsetIndexOut, 2);
   
   % If there isn't onset event for note-ma note, continue
   if (nSrcNotes == 0 && nOutNotes == 0)
      continue; 
   end
   
   % Equalize onsetEvent dimension
   if (nSrcNotes == 0)
      onsetIndexSrc = 0;
   end
   if(nOutNotes == 0)
      onsetIndexOut = 0;
   end
   
   % Search the frame distance between Source onset event 
   % and Out onset event. For each Source event, match the Out event with
   % minimum distance.
   % This distance will be used for correct note and false negative
   % research.
   if(nSrcNotes ~= 0)
       for i = 1:size(onsetIndexSrc, 2)
          [~, minIndex] = min(abs(onsetIndexSrc(i) - onsetIndexOut));  
          deltaSrc2Out(eventNumberSrc) = onsetIndexOut(minIndex) - onsetIndexSrc(i);
          eventNumberSrc = eventNumberSrc+1;
       end
   end
   
   % Search the frame distance between Out onset event 
   % and Source onset event. For each Out event, match the Source event with
   % minimum distance.
   % This distance will be used for false positive research.
   if(nOutNotes ~= 0)
       for i = 1:size(onsetIndexOut, 2)
          [~, minIndex] = min(abs(onsetIndexOut(i) - onsetIndexSrc));  
          deltaOut2Src(eventNumberOut) = onsetIndexOut(i) - onsetIndexSrc(minIndex);
          eventNumberOut = eventNumberOut+1;
       end
   end
end

% realign midi
offset = mode([deltaSrc2Out;deltaOut2Src]);
if(offset > 0)
    sourceActivityMatrix = [zeros(size(sourceActivityMatrix, 1), offset) sourceActivityMatrix];
elseif(offset < 0)
    outActivityMatrix = [zeros(size(outActivityMatrix, 1), -offset) outActivityMatrix];
end

% Equalize Activity Matrix dimension
nSrcFrame = size(sourceActivityMatrix, 2);
nOutFrame = size(outActivityMatrix, 2);
if (nSrcFrame > nOutFrame)
    outActivityMatrix = padarray(outActivityMatrix, [0 nSrcFrame - nOutFrame], 'post');
else
    sourceActivityMatrix = padarray(sourceActivityMatrix, [0 nOutFrame - nSrcFrame], 'post');
end
nFrame = max(nSrcFrame, nOutFrame);

numActivitySource = zeros(nFrame, 1);
numActivityOut = zeros(nFrame, 1);
correctlyTranscribedInFrame = zeros(nFrame, 1);
falseNegativeInFrame = zeros(nFrame, 1);
falsePositiveInFrame = zeros(nFrame, 1);

% Compute Dixon Accuracy frame by frame.
% To analyze note activity the frame of source activity
% and out activity were summed with diverse coefficent 
activityCount = 1;
for i=1:nFrame
    delta = sourceActivityMatrix(:, i) + 2*outActivityMatrix(:,i);
    
    nActivity = size(delta(delta~=0),1);
    if(nActivity ~= 0)
        activitySource = sourceActivityMatrix(:, i);
        activityOut = outActivityMatrix(:, i);
        numActivitySource(i) = size(activitySource(activitySource == 1), 1);
        numActivityOut(i) = size(activityOut(activityOut == 1), 1); 
        
        correctlyTranscribedInFrame(i) = size(delta(delta==3),1);
        falseNegativeInFrame(i) = size(delta(delta==1),1);
        falsePositiveInFrame(i) = size(delta(delta==2),1);
    end
end
Dixon = 100 * (sum(correctlyTranscribedInFrame) / sum(correctlyTranscribedInFrame + falseNegativeInFrame + falsePositiveInFrame));
Esub  = 100 * (sum(min(numActivitySource, numActivityOut) - correctlyTranscribedInFrame)/sum(numActivitySource));
Efn   = 100 * (sum(max(0, numActivitySource - numActivityOut))/sum(numActivitySource));
Efp   = 100 * (sum(max(0, numActivityOut - numActivitySource))/sum(numActivitySource));
Etot  = Esub + Efn + Efp;


