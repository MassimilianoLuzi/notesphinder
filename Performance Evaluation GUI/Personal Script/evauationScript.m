% Copyright (c) 2015, 2016 Massimiliano Luzi, Mario Antonelli and Antonello Rizzi
% University of Rome "La Sapienza"
%
% massimiliano.luzi@uniroma1.it
% mario.antonelli@gmail.com 
% antonello.rizzi@uniroma1.it 
%
% This file is part of NotesPHInder.
%
% NotesPHInder is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
% 
% NotesPHInder is distributed in the hope that it will be useful. 
% IT IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
% INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
% PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
% HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
% CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
% OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 
% See the GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with Nome-Programma.If not, see<http://www.gnu.org/licenses/>.

close all;
clear all;
clc

addpath(genpath('C:\Users\Massimiliano\Google Drive\UniversitÓ\Dottorato\AMT\Nuovi Test\Midi compare\matlab-midi-master'));

addpath('C:\Users\Massimiliano\Google Drive\UniversitÓ\Dottorato\AMT\Nuovi Test\Midi compare');

nameInstrument = {'Steinway Grandpiano';
                  'Yamaha C2';
                  'Disklavier'};

% Set file path of output files
sourceMidiPath = 'C:\Users\Massimiliano\Google Drive\UniversitÓ\Dottorato\AMT\Nuovi Test\Piano\Ref Midi\Midi Test 1min';   
sourceMidiFileNames = dir(fullfile(sourceMidiPath, '*.mid'));
% Set file path of out midi files           
outMidiPath = {'Steinway Grandpiano'; 
               'Yamaha C2';
               'Disklavier'};    

outMidiFileNames = {dir(fullfile(outMidiPath{1}, '*.mid'));
          dir(fullfile(outMidiPath{2}, '*.mid'));
          dir(fullfile(outMidiPath{3}, '*.mid'))};
      
results(3) = struct(...
    'InstrumentName', [], ...
    'FrameResults',   zeros(10, 5),...
    'OnsetResults',   zeros(10, 4));
      
for i=1:3
    numSourceMidi = length(sourceMidiFileNames);
    numOutMidi = length(outMidiFileNames{i});
    if numSourceMidi ~= 10||numOutMidi ~= 10||numSourceMidi ~= numOutMidi
        error('The number of midi files is less than 10');
    end
    
    results(i).InstrumentName = nameInstrument{i};
    for n = 1:numOutMidi   
        sourcePath = fullfile(sourceMidiPath, sourceMidiFileNames(n).name);
        outPath = fullfile(outMidiPath{i}, outMidiFileNames{i}(n).name);
        
        [~, sourceName, ~] = fileparts(sourcePath);
        [~, outName, ~] = fileparts(outPath);
        fprintf('Instrument: %s \n \t source: %s \n \t out: %s \n',nameInstrument{i},sourceName, outName); 
        
        sourceMidi = readmidi(sourcePath);
        outMidi = readmidi(outPath);
        
        [frameAcc, Esub, Efn, Efp, Etot] = frameLevelCompare(sourceMidi, outMidi);
        results(i).FrameResults(n,1) = frameAcc;
        results(i).FrameResults(n,2) = Esub;
        results(i).FrameResults(n,3) = Efn;
        results(i).FrameResults(n,4) = Efp;
        results(i).FrameResults(n,5) = Etot;
        
        [onsetAcc, Precision, Recall, F_measure] = onsetLevelCompare(sourceMidi, outMidi);
        results(i).OnsetResults(n,1) = onsetAcc;
        results(i).OnsetResults(n,2) = Precision;
        results(i).OnsetResults(n,3) = Recall;
        results(i).OnsetResults(n,4) = F_measure; 
    end
    table = {'Frame Level', 'frameAcc', 'Esub', 'Efn', 'Efp', 'Etot'};
    xlswrite('results', table, nameInstrument{i}, 'A1');
    xlswrite('results', results(i).FrameResults, nameInstrument{i}, 'B2');
    xlswrite('results', {'AVG'}, nameInstrument{i}, 'A12');
    xlswrite('results', mean(results(i).FrameResults,1), nameInstrument{i}, 'B12');
    
    table = {'Onset Level', 'onsetAcc', 'Precision', 'Recall', 'F_measure'};
    xlswrite('results', table, nameInstrument{i}, 'H1');
    xlswrite('results', results(i).OnsetResults, nameInstrument{i}, 'I2');
    xlswrite('results', {'AVG'}, nameInstrument{i}, 'H12');
    xlswrite('results', mean(results(i).OnsetResults,1), nameInstrument{i}, 'I12');
end

save('results.mat');

      