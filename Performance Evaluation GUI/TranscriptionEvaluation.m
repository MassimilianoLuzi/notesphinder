% Copyright (c) 2015, 2016 Massimiliano Luzi, Mario Antonelli and Antonello Rizzi
% University of Rome "La Sapienza"
%
% massimiliano.luzi@uniroma1.it
% mario.antonelli@gmail.com 
% antonello.rizzi@uniroma1.it 
%
% This file is part of NotesPHInder.
%
% NotesPHInder is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
% 
% NotesPHInder is distributed in the hope that it will be useful. 
% IT IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
% INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
% PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
% HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
% CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
% OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 
% See the GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with NotesPHInder.If not, see<http://www.gnu.org/licenses/>.

function varargout = TranscriptionEvaluation(varargin)
% TRANSCRIPTIONEVALUATION MATLAB code for TranscriptionEvaluation.fig
%      TRANSCRIPTIONEVALUATION, by itself, creates a new TRANSCRIPTIONEVALUATION or raises the existing
%      singleton*.
%
%      H = TRANSCRIPTIONEVALUATION returns the handle to a new TRANSCRIPTIONEVALUATION or the handle to
%      the existing singleton*.
%
%      TRANSCRIPTIONEVALUATION('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in TRANSCRIPTIONEVALUATION.M with the given input arguments.
%
%      TRANSCRIPTIONEVALUATION('Property','Value',...) creates a new TRANSCRIPTIONEVALUATION or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before TranscriptionEvaluation_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to TranscriptionEvaluation_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help TranscriptionEvaluation

% Last Modified by GUIDE v2.5 27-Jan-2016 17:52:19

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @TranscriptionEvaluation_OpeningFcn, ...
                   'gui_OutputFcn',  @TranscriptionEvaluation_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before TranscriptionEvaluation is made visible.
function TranscriptionEvaluation_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to TranscriptionEvaluation (see VARARGIN)

% Choose default command line output for TranscriptionEvaluation
addpath(genpath('Functions'));

handles.output = hObject;

handles.midiSource = [];

handles.midiOut = [];

handles.frameMetricOut = struct(...
    'Accuracy', [],...
    'Esub',     [],...
    'Efn',      [],...
    'Efp',      [],...
    'Etot',     []...
    );

handles.onsetMetricOut = struct(...
    'Accuracy',     [],...
    'Precision',    [],...
    'Recall',       [],...
    'fMeasure',     []...
    );

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes TranscriptionEvaluation wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = TranscriptionEvaluation_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in refMidiButton.
function refMidiButton_Callback(hObject, eventdata, handles)
% hObject    handle to refMidiButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
[sourceFileName, sourcePath, ] = uigetfile('*.mid', 'Select Source Midi', '..\');
handles.midiSource = readmidi(strcat(sourcePath,sourceFileName));

set(handles.sourceMidiText, 'String', sourceFileName);

% Update handles structure
guidata(hObject, handles);

% --- Executes on button press in outMidiButton.
function outMidiButton_Callback(hObject, eventdata, handles)
% hObject    handle to outMidiButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
[outFileName, outPath, ] = uigetfile('*.mid', 'Select Out Midi', '..\');
handles.midiOut = readmidi(strcat(outPath, outFileName));
set(handles.outMidiText, 'String', outFileName);
[handles.dataForStore.path, handles.dataForStore.name] = fileparts(strcat(outPath, outFileName))
handles.outPath = outPath;
% Update handles structure
guidata(hObject, handles);

function sourceMidiText_Callback(hObject, eventdata, handles)
% hObject    handle to sourceMidiText (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of sourceMidiText as text
%        str2double(get(hObject,'String')) returns contents of sourceMidiText as a double


% --- Executes during object creation, after setting all properties.
function sourceMidiText_CreateFcn(hObject, eventdata, handles)
% hObject    handle to sourceMidiText (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function outMidiText_Callback(hObject, eventdata, handles)
% hObject    handle to outMidiText (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of outMidiText as text
%        str2double(get(hObject,'String')) returns contents of outMidiText as a double


% --- Executes during object creation, after setting all properties.
function outMidiText_CreateFcn(hObject, eventdata, handles)
% hObject    handle to outMidiText (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in selectMetric.
function selectMetric_Callback(hObject, eventdata, handles)
% hObject    handle to selectMetric (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns selectMetric contents as cell array
%        contents{get(hObject,'Value')} returns selected item from selectMetric


% --- Executes during object creation, after setting all properties.
function selectMetric_CreateFcn(hObject, eventdata, handles)
% hObject    handle to selectMetric (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function frameAcc_Callback(hObject, eventdata, handles)
% hObject    handle to frameAcc (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of frameAcc as text
%        str2double(get(hObject,'String')) returns contents of frameAcc as a double


% --- Executes during object creation, after setting all properties.
function frameAcc_CreateFcn(hObject, eventdata, handles)
% hObject    handle to frameAcc (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function subErr_Callback(hObject, eventdata, handles)
% hObject    handle to subErr (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of subErr as text
%        str2double(get(hObject,'String')) returns contents of subErr as a double


% --- Executes during object creation, after setting all properties.
function subErr_CreateFcn(hObject, eventdata, handles)
% hObject    handle to subErr (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function fpErr_Callback(hObject, eventdata, handles)
% hObject    handle to fpErr (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of fpErr as text
%        str2double(get(hObject,'String')) returns contents of fpErr as a double


% --- Executes during object creation, after setting all properties.
function fpErr_CreateFcn(hObject, eventdata, handles)
% hObject    handle to fpErr (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function fnErr_Callback(hObject, eventdata, handles)
% hObject    handle to fnErr (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of fnErr as text
%        str2double(get(hObject,'String')) returns contents of fnErr as a double


% --- Executes during object creation, after setting all properties.
function fnErr_CreateFcn(hObject, eventdata, handles)
% hObject    handle to fnErr (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function totErr_Callback(hObject, eventdata, handles)
% hObject    handle to totErr (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of totErr as text
%        str2double(get(hObject,'String')) returns contents of totErr as a double


% --- Executes during object creation, after setting all properties.
function totErr_CreateFcn(hObject, eventdata, handles)
% hObject    handle to totErr (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function onsetAcc_Callback(hObject, eventdata, handles)
% hObject    handle to onsetAcc (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of onsetAcc as text
%        str2double(get(hObject,'String')) returns contents of onsetAcc as a double


% --- Executes during object creation, after setting all properties.
function onsetAcc_CreateFcn(hObject, eventdata, handles)
% hObject    handle to onsetAcc (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function precision_Callback(hObject, eventdata, handles)
% hObject    handle to precision (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of precision as text
%        str2double(get(hObject,'String')) returns contents of precision as a double


% --- Executes during object creation, after setting all properties.
function precision_CreateFcn(hObject, eventdata, handles)
% hObject    handle to precision (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function recall_Callback(hObject, eventdata, handles)
% hObject    handle to recall (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of recall as text
%        str2double(get(hObject,'String')) returns contents of recall as a double


% --- Executes during object creation, after setting all properties.
function recall_CreateFcn(hObject, eventdata, handles)
% hObject    handle to recall (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function fMeasure_Callback(hObject, eventdata, handles)
% hObject    handle to fMeasure (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of fMeasure as text
%        str2double(get(hObject,'String')) returns contents of fMeasure as a double


% --- Executes during object creation, after setting all properties.
function fMeasure_CreateFcn(hObject, eventdata, handles)
% hObject    handle to fMeasure (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in compareButton.
function compareButton_Callback(hObject, eventdata, handles)
% hObject    handle to compareButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

[handles.onsetMetricOut.Accuracy,       ...
    handles.onsetMetricOut.Precision,   ...
    handles.onsetMetricOut.Recall,      ...
    handles.onsetMetricOut.fMeasure] = onsetLevelCompare(handles.midiSource, handles.midiOut);

set(handles.onsetAcc, 'String', sprintf('%.2f \%', handles.onsetMetricOut.Accuracy));
set(handles.precision, 'String', sprintf('%.2f \%', handles.onsetMetricOut.Precision));
set(handles.recall, 'String', sprintf('%.2f \%', handles.onsetMetricOut.Recall));
set(handles.fMeasure, 'String', sprintf('%.2f \%', handles.onsetMetricOut.fMeasure));


[handles.frameMetricOut.Accuracy,   ...
    handles.frameMetricOut.Esub,    ...
    handles.frameMetricOut.Efn,     ...
    handles.frameMetricOut.Efp,     ...
    handles.frameMetricOut.Etot,    ...
    sourceActivityMatrix,           ...
    outActivityMatrix] = frameLevelCompare(handles.midiSource, handles.midiOut);

set(handles.frameAcc, 'String', sprintf('%.2f %', handles.frameMetricOut.Accuracy));
set(handles.subErr, 'String', sprintf('%.2f %', handles.frameMetricOut.Esub));
set(handles.fpErr, 'String', sprintf('%.2f %', handles.frameMetricOut.Efp));
set(handles.fnErr, 'String', sprintf('%.2f %', handles.frameMetricOut.Efn));
set(handles.totErr, 'String', sprintf('%.2f %', handles.frameMetricOut.Etot));

axes(handles.refMidiPlot);
imagesc(sourceActivityMatrix);

axes(handles.outMidiPlot);
imagesc(outActivityMatrix);

outMatPath = strcat(handles.dataForStore.path, '\', handles.dataForStore.name);
save(outMatPath, '-struct', 'handles', 'frameMetricOut', 'onsetMetricOut');
% Update handles structure
guidata(hObject, handles);

% --- Executes on button press in clearButton.
function clearButton_Callback(hObject, eventdata, handles)
% hObject    handle to clearButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set(handles.onsetAcc, 'String', []);
set(handles.precision, 'String', []);
set(handles.recall, 'String', []);
set(handles.fMeasure, 'String', []);

set(handles.frameAcc, 'String', []);
set(handles.subErr, 'String', []);
set(handles.fpErr, 'String', []);
set(handles.fnErr, 'String', []);
set(handles.totErr, 'String', []);

handles.sourceOut = [];
set(handles.sourceMidiText, 'String', []);

handles.midiOut = [];
set(handles.outMidiText, 'String', []);

cla(handles.refMidiPlot, 'reset');

cla(handles.outMidiPlot, 'reset');

% Update handles structure
guidata(hObject, handles);

