Copyright (c) 2015, 2016 Massimiliano Luzi, Mario Antonelli and Antonello Rizzi, University of Rome "La Sapienza"

massimiliano.luzi@uniroma1.it

mario.antonelli@gmail.com 

antonello.rizzi@uniroma1.it 

This file is part of NotesPHInder.

NotesPHInder is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.
 
NotesPHInder is distributed in the hope that it will be useful. 
IT IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with NotesPHInder.If not, see<http://www.gnu.org/licenses/>.


Notes PHInder ver. 1.0:
Notes PHInder is a Windows software for automatic music transcription of monotimbral music.
It receives as input a 8KHz, 16bps mono channel WAVE file and returns a MIDI file encoding the score of the input 
WAVE file.

Notes PHInder uses a dictionary based spectrogram factorization metohd for performing notes transcription.
In particular, it uses a two-step Non-negative Matrix Decomposition where an iterative dictionary pruning and a L2
regularization are used in order to enhance the sparsity of the notes activity and to improving the transcription accuracy.
Furthermore, Notes PHInder allows to create a custom dictionary by performing a very user-friendly learning procedure upon the current instrument.   

###Transcription###

1) Load the *.wav file to transcribe by clicking on 'Open Wave' or on the progress bar: 
* The wave file has to be a MONO channel PCM WAVE file sampled at 8KHz and with 16bps.

2) Enable/Disable the HIGH DYNAMIC RANGE COMPRESSION Pre-processing. If enabled, it allows to significantly improve the transcription accuracy.

3) Select the Instrument Dictionary.
* Some experimental dictionaries are provided. The user is encouraged to create its own dictionaries.

4) Click Transcribe to start the transcription.

5) Save the returned MIDI file where you want.

6) Compare the original music file and the transcribed score by clicking on 'Play Wave' and 'Play Midi' buttons.

7) See the notes activity and the input spectrogram by clicking on 'Show Notes Activity' and 'Show Spectrum', respectively.


###Instrument Learning###

1) Record a certain number L of different monophonic notes from your instrument. You should record the most used used notes of your instrument. For example for piano the twelve notes of the fourth octave could be a good choise. Each recording has to be encoded in a MONO channel WAVE file sampled at 8KHz and 16bps.

2) Collect the notes sample in a single directory @NOTES_SAMPLE

3) Click on 'Create Dictionary'

4) Click on Source and browse to the directory @NOTES_SAMPLE

5) Click on 'Start Learning'

6) Save the new dictionary on the suggested directory.


### References ###
This work is related to the paper "Instrument Learning and Notes Recognition by Sparse NMD for Automatic Polyphonic Music Transcription" authored by Antonello Rizzi, Mario Antonelli and Massimiliano Luzi and submitted to "IEEE Transactions on Multimedia". Each use of this software must cite this paper.

Massimiliano Luzi - massimiliano.luzi@uniroma1.it

Mario Antonelli - mario.antonelli@gmail.com

Antonello Rizzi - antonello.rizzi@uniroma1.it


University of Roma "La Sapienza" - Via Eudossiana 18, 00184 Roma