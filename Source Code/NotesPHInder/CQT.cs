﻿// Copyright (c) 2015, 2016 Massimiliano Luzi, Mario Antonelli and Antonello Rizzi
// University of Rome "La Sapienza"
//
// massimiliano.luzi@uniroma1.it
// mario.antonelli@gmail.com 
// antonello.rizzi@uniroma1.it 
//
// This file is part of NotesPHInder.
//
// NotesPHInder is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// NotesPHInder is distributed in the hope that it will be useful. 
// IT IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
// PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
// CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with NotesPHInder.If not, see<http://www.gnu.org/licenses/>.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace NotesPHInder
{
    /// <summary>
    /// Class for Constant Q Transform computation
    /// </summary>
    public class CQT
    {
        #region Global Variables
        
        // CQT variables        
        BackgroundWorker worker;     
        int windowSize;      
        int nWindows;
        int numOfFreqs;
        double toneSpacing;
        double[,] cqtTransform;

        // LogKernelGenerator variables 
        double[][] kernCos;
        double[][] kernSin;
        double[] freqs;
        static int[] windowLenght;

        bool constantWind = false;
        
        #endregion     

        #region Constructor

        /// <summary>
        /// Cqt constructor
        /// </summary>
        /// <param name="sampleRate">Sample Rate of wav file</param>
        /// <param name="nFreqs">Number of frequencies</param>
        /// <param name="maxWindowSize">Max length of window</param>
        /// <param name="Q">Q value</param>
        /// <param name="tSpacing">Tone spacing factor</param>
        public CQT(int sampleRate, int nFreqs, int maxWindowSize, int Q, double tSpacing)
        {
            windowSize = maxWindowSize;
            numOfFreqs = nFreqs;
            toneSpacing = tSpacing;
            GetLogKernel(sampleRate, nFreqs, maxWindowSize, Q, tSpacing);
            worker = null;
        }
        #endregion

        #region Public Properties

        /// <summary>
        /// Get the number of windows in audio file
        /// </summary>
        public int NumberOfWindows
        {
            get
            {
                return nWindows;
            }
        }

        /// <summary>
        /// Get the total number of Frequencies
        /// </summary>
        public int NumberOfFrequencies
        {
            get
            {
                return numOfFreqs;
            }
        }

        /// <summary>
        /// Get the Cosine Kernel Matrix
        /// </summary>
        public double[][] KernelCos
        {
            get
            {
                return kernCos;
            }
        }

        /// <summary>
        ///  Get the Sine Kernel Matrix
        /// </summary>
        public double[][] KenrelSin
        {
            get
            {
                return kernSin;
            }
        }

        /// <summary>
        /// Get the Windows' Lenght for all the frequencies
        /// </summary>
        public int[] WindowLenght
        {
            get
            {
                return windowLenght;
            }
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Compute the Kernel for CQT computation
        /// </summary>
        /// <param name="sampleRate">Sample rate in Hz</param>
        /// <param name="numberOfFrequences">Total number of frequencies</param>
        /// <param name="maxWindowsSize">Max size of window</param>
        /// <param name="Q">Q value</param>
        /// <param name="toneSpacing">2^(1/B) where B = bin per octave</param>
        public void GetLogKernel(
            int sampleRate,
            int numberOfFrequences,
            int maxWindowsSize,
            int Q, double toneSpacing)
        {
            double minDigfreq = Constants.twoPi * Constants.minFrequence / sampleRate;

            List<double> freqsApp = new List<double>();
            List<double> digFreqsApp = new List<double>();
            for (int index = 0; index < numberOfFrequences; index++)
            {
                double f = Constants.minFrequence * Math.Pow(toneSpacing, index);
                if (f < sampleRate / 2)
                {
                    freqsApp.Add(f);
                    digFreqsApp.Add(f * Constants.twoPi / sampleRate);
                }
                else
                    break;
            }

            freqs = freqsApp.ToArray();
            double[] digFreqs = digFreqsApp.ToArray();

            freqsApp.Clear();
            digFreqsApp.Clear();

            kernCos = new double[numberOfFrequences][];
            kernSin = new double[numberOfFrequences][];

            windowLenght = new int[numberOfFrequences];

            for (int f = 0; f < numberOfFrequences; f++)
            {
                double[] wind;

                // Discerne between constant window or variable length windows
                if (constantWind == true)
                {
                    wind = HammingWindow.GetWindow(maxWindowsSize);
                    windowLenght[f] = wind.Length;
                }
                else
                {
                    wind = HammingWindow.GetWindow(Q, freqs[f], sampleRate);
                    windowLenght[f] = wind.Length;

                    if (windowLenght[f] > maxWindowsSize)
                    {
                        wind = HammingWindow.GetWindow(maxWindowsSize);
                        windowLenght[f] = maxWindowsSize;
                    }
                }

                kernCos[f] = new double[wind.Length];
                kernSin[f] = new double[wind.Length];

                // Compute kernels
                for (int index = 0; index < wind.Length; index++)
                {
                    kernCos[f][index] = (1.0 / wind.Length) * Math.Cos(digFreqs[f] * ((-wind.Length / 2) + index +1)) * wind[index];
                    kernSin[f][index] = (1.0 / wind.Length) * Math.Sin(digFreqs[f] * ((-wind.Length / 2) + index +1)) * wind[index];
                }
            }
        }

        /// <summary>
        /// Compute the CQT Transform
        /// </summary>
        /// <param name="samples">Samples to tranform</param>
        /// <param name="hopSize">Size of the hop in sample</param>
        /// <param name="wrk">Background worker</param>
        /// <returns>Constant Q Transform of samples</returns>
        public double[,] GetTranform(double[] samples, int hopSize, BackgroundWorker wrk)
        {
            string[] trainingProgState = new[] {"local", ""};
            worker = wrk;
            
            int numberOfHops = 1 + (int)Math.Floor((double)(samples.Length - windowSize) / hopSize);
            
            cqtTransform = new double[numOfFreqs, numberOfHops];

            int[] windowLength = WindowLenght;
            int offset; 

            int perc = 0;

            for (int hop = 0; hop < numberOfHops; hop++)
            {
                int startOfFrame = (int)(hop * hopSize);
                
                if ((worker != null) && (worker.CancellationPending))
                {
                    break;
                }
                else
                {
                    for (int f = 0; f < numOfFreqs; f++)
                    {
                        double reOinfil = 0.0;
                        double imOinfil = 0.0;

                        // Set the offset for alignment of windows
                        // Center Alignment
                        offset = (windowSize - windowLength[f])/2;

                        //// Right Alignment
                        // offset = (windowSize - windowLength[f]);  

                        //// Left Alignment
                        // offset = 0;                   

                        // Compute real part and immaginary part of the window.
                        for (int iii = 0; iii < windowLength[f]; iii++)
                        {
                            reOinfil += samples[startOfFrame + iii  + offset] * KernelCos[f][iii];
                            imOinfil += samples[startOfFrame + iii  + offset] * KenrelSin[f][iii];
                        }
                        // Store the absolute value of tranform
                        cqtTransform[f, hop] = Math.Sqrt(reOinfil * reOinfil + imOinfil * imOinfil);
                    }
                    // Report Progress
                    perc = (int)Math.Floor((100.0 * (hop + 1)) / (numberOfHops));
                    worker.ReportProgress(perc, trainingProgState);
                }
            }
            nWindows = numberOfHops;
            return cqtTransform;
        }

        #endregion
    }
}
