﻿// Copyright (c) 2015, 2016 Massimiliano Luzi, Mario Antonelli and Antonello Rizzi
// University of Rome "La Sapienza"
//
// massimiliano.luzi@uniroma1.it
// mario.antonelli@gmail.com 
// antonello.rizzi@uniroma1.it 
//
// This file is part of NotesPHInder.
//
// NotesPHInder is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// NotesPHInder is distributed in the hope that it will be useful. 
// IT IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
// PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
// CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with NotesPHInder.If not, see<http://www.gnu.org/licenses/>.

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ILNumerics;
using ILNumerics.Drawing;
using ILNumerics.Drawing.Plotting;
using System.Threading;

namespace NotesPHInder
{
    /// <summary>
    /// Class for visualization of Constant Q Transform Graphic
    /// </summary>
    public partial class CqtImage : Form
    {
        float[,] matrix;
        double toneSpacing;
        int binsPerOctave;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="transform">Constant Q Transform</param>
        /// <param name="tSpacing">Tone spacing factor</param>
        /// <param name="bpOctave">Bins per octave</param>
        public CqtImage(double[,] transform, double tSpacing, int bpOctave)
        {
            InitializeComponent();
            
            // Store initial data
            toneSpacing = tSpacing;
            binsPerOctave = bpOctave;

            int numberOfFrequences = transform.GetLength(0);
            int numberOfTimes = transform.GetLength(1);

            matrix = new float[numberOfTimes, numberOfFrequences];

            // Convert double to float
            for (int i = 0; i < numberOfFrequences; i++)
            {
                for (int j = 0; j < numberOfTimes; j++)
                {
                    matrix[j, i] = (float)(transform[i, j]);
                }
            }
        }
        
        /// <summary>
        /// Build the Constant Q Transform graphic
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ilPanel1_Load(object sender, EventArgs e)
        {
            // Set scene
            var scene = new ILScene();
            ILSurface surface = new ILSurface(matrix, colormap: Colormaps.Jet);
            surface.Wireframe.Visible = false;
            var CqtPlot = scene.Add(new ILPlotCube());

            CqtPlot.Add(surface);
            CqtPlot.Axes.XAxis.Label.Text = "Time";
            CqtPlot.Axes.YAxis.Label.Text = "Notes";
            
            int numberOfFrequences = matrix.GetLength(1);
            int numberOfTimes = matrix.GetLength(0);
            int octave;
            ILTick tick;

            // Set axis ticks
            for (int f = 0; f < numberOfFrequences; f++)
            {
                octave = (f / binsPerOctave) + 1;
                if (f % (binsPerOctave / 12) == 0)
                {
                    switch ((f / (binsPerOctave / 12)) % 12)
                    {
                        case 0:
                            tick = CqtPlot.Axes.YAxis.Ticks.Add(f, "C" + octave.ToString());
                            tick.Label.Anchor = new PointF(1f, 0.5f);
                            tick.Label.Font = new Font(FontFamily.GenericSansSerif, 6);
                            break;

                        case 1:
                            tick = CqtPlot.Axes.YAxis.Ticks.Add(f, "C#" + octave.ToString());
                            tick.Label.Anchor = new PointF(1f, 0.5f);
                            tick.Label.Font = new Font(FontFamily.GenericSansSerif, 6);
                            break;

                        case 2:
                            tick = CqtPlot.Axes.YAxis.Ticks.Add(f, "D" + octave.ToString());
                            tick.Label.Anchor = new PointF(1f, 0.5f);
                            tick.Label.Font = new Font(FontFamily.GenericSansSerif, 6);
                            break;

                        case 3:
                            tick = CqtPlot.Axes.YAxis.Ticks.Add(f, "D#" + octave.ToString());
                            tick.Label.Anchor = new PointF(1f, 0.5f);
                            tick.Label.Font = new Font(FontFamily.GenericSansSerif, 6);
                            break;

                        case 4:
                            tick = CqtPlot.Axes.YAxis.Ticks.Add(f, "E" + octave.ToString());
                            tick.Label.Anchor = new PointF(1f, 0.5f);
                            tick.Label.Font = new Font(FontFamily.GenericSansSerif, 6);
                            break;

                        case 5:
                            tick = CqtPlot.Axes.YAxis.Ticks.Add(f, "F" + octave.ToString());
                            tick.Label.Anchor = new PointF(1f, 0.5f);
                            tick.Label.Font = new Font(FontFamily.GenericSansSerif, 6);
                            break;

                        case 6:
                            tick = CqtPlot.Axes.YAxis.Ticks.Add(f, "F#" + octave.ToString());
                            tick.Label.Anchor = new PointF(1f, 0.5f);
                            tick.Label.Font = new Font(FontFamily.GenericSansSerif, 6);
                            break;

                        case 7:
                            tick = CqtPlot.Axes.YAxis.Ticks.Add(f, "G" + octave.ToString());
                            tick.Label.Anchor = new PointF(1f, 0.5f);
                            tick.Label.Font = new Font(FontFamily.GenericSansSerif, 6);
                            break;

                        case 8:
                            tick = CqtPlot.Axes.YAxis.Ticks.Add(f, "G#" + octave.ToString());
                            tick.Label.Anchor = new PointF(1f, 0.5f);
                            tick.Label.Font = new Font(FontFamily.GenericSansSerif, 6);
                            break;

                        case 9:
                            tick = CqtPlot.Axes.YAxis.Ticks.Add(f, "A" + octave.ToString());
                            tick.Label.Anchor = new PointF(1f, 0.5f);
                            tick.Label.Font = new Font(FontFamily.GenericSansSerif, 6);
                            break;

                        case 10:
                            tick = CqtPlot.Axes.YAxis.Ticks.Add(f, "A#" + octave.ToString());
                            tick.Label.Anchor = new PointF(1f, 0.5f);
                            tick.Label.Font = new Font(FontFamily.GenericSansSerif, 6);
                            break;

                        case 11:
                            tick = CqtPlot.Axes.YAxis.Ticks.Add(f, "B" + octave.ToString());
                            tick.Label.Anchor = new PointF(1f, 0.5f);
                            tick.Label.Font = new Font(FontFamily.GenericSansSerif, 6);
                            break;
                            
                        default:
                            break;
                    }
                }
            }
            
            ilPanel1.Scene = scene;          
        }
    }
}
