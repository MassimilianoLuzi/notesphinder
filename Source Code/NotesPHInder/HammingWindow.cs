﻿// Copyright (c) 2015, 2016 Massimiliano Luzi, Mario Antonelli and Antonello Rizzi
// University of Rome "La Sapienza"
//
// massimiliano.luzi@uniroma1.it
// mario.antonelli@gmail.com 
// antonello.rizzi@uniroma1.it 
//
// This file is part of NotesPHInder.
//
// NotesPHInder is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// NotesPHInder is distributed in the hope that it will be useful. 
// IT IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
// PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
// CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with NotesPHInder.If not, see<http://www.gnu.org/licenses/>.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NotesPHInder
{
    /// <summary>
    /// Definition of Haminng Window
    /// </summary>
    static class HammingWindow
    {
        #region Local Variables

        // Alpha parameter for Hamming Window
        private static readonly double alpha = 25.0 / 46.0;

        #endregion

        #region Public Methods

        /// <summary>
        /// Get the Hamming window of length 'size'.
        /// </summary>
        /// <param name="size">Length of window</param>
        /// <returns>Hamming Window of length size</returns>
        public static double[] GetWindow(int size)
        {
            double[] wind = new double[size];           
            for (int index = 0; index < size; index++)
            {
                wind[index] = alpha - (1 - alpha) * Math.Cos(2 * Math.PI * index / (size - 1));
            }

            return wind;
        }

        /// <summary>
        /// Get the Hamming window of length given by: (sampleRate*Q)/frequence.
        /// </summary>
        /// <param name="Q"></param>
        /// <param name="frequence"></param>
        /// <param name="sampleRate"></param>
        /// <returns></returns>
        public static double[] GetWindow(int Q, double frequence, int sampleRate)
        {                        
            int size = (int)Math.Floor((sampleRate * Q) / frequence);
            double[] wind = new double[size];
                                
            for (int index = 0; index < size; index++)
            {
                wind[index] = alpha - (1 - alpha) * Math.Cos(2 * Math.PI * index / (size - 1));
            }    
                                       
        return wind;
        }

        #endregion
    }
}
