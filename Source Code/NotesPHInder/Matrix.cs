﻿// Copyright (c) 2015, 2016 Massimiliano Luzi, Mario Antonelli and Antonello Rizzi
// University of Rome "La Sapienza"
//
// massimiliano.luzi@uniroma1.it
// mario.antonelli@gmail.com 
// antonello.rizzi@uniroma1.it 
//
// This file is part of NotesPHInder.
//
// NotesPHInder is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// NotesPHInder is distributed in the hope that it will be useful. 
// IT IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
// PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
// CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with NotesPHInder.If not, see<http://www.gnu.org/licenses/>.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NotesPHInder
{
    /// <summary>
    /// Definition of Type 'Matrix' with redefinition of operands and methods
    /// </summary>
    public class Matrix
    {
        #region Global Variables

        /// <summary>
        /// matrix variable
        /// </summary>
        public double[,] matrix;
        
        #endregion

        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="nRow">Number of Rows in matrix</param>
        /// <param name="nCol">Number of Columns in matrix</param>
        public Matrix(int nRow, int nCol)
        {
            matrix = new double[nRow, nCol];
        }

        #endregion

        #region Public Properties
        
        /// <summary>
        /// Get and set the matrix value
        /// </summary>
        public double[,] Value
        {
            get
            {
                return matrix;
            }

            set
            {
                matrix = value;
            }
        }

        /// <summary>
        /// Get the numer of Rows in matrix
        /// </summary>
        public int NumberOfRows
        {
            get
            {
                return matrix.GetLength(0);
            }
        }

        /// <summary>
        /// Get the number of Columns in matrix
        /// </summary>
        public int NumberOfCol
        {
            get
            {
                return matrix.GetLength(1);
            }
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Static method. Normalize a double[,] variable with linear normalization
        /// </summary>
        /// <param name="doubleMatrix">Matrix as double[,] type</param>
        /// <returns>Linear normalized matrix as double[,]</returns>
        public static double[,] LinearNormalizeMatrix(double[,] doubleMatrix)
        {
            double max = double.NegativeInfinity;

            // Extract dimensions
            int nRow = doubleMatrix.GetLength(0);
            int nCol = doubleMatrix.GetLength(1);

            double[,] normalizedMatrix = new double[nRow, nCol];

            // find the maximum value
            for (int i = 0; i < nRow; i++)
            {
                for (int j = 0; j < nCol; j++)
                {
                    if (doubleMatrix[i, j] > max)
                        max = doubleMatrix[i, j];
                }
            }

            // compute the linear normalization dividing by max value
            for (int i = 0; i < nRow; i++)
            {
                for (int j = 0; j < nCol; j++)
                {
                    normalizedMatrix[i, j] = Math.Max((doubleMatrix[i, j] / max), Constants.eps);
                }
            }
            return normalizedMatrix;
        }

        /// <summary>
        /// Affine normalizazion for double[,] matrix.
        /// </summary>
        /// <param name="matrix">Matrix as double[,] type</param>
        /// <returns>Affine normalized matrix as double[,]</returns>
        public static double[,] AffineNormalizeMatrix(double[,] doubleMatrix)
        {
            double min = double.PositiveInfinity;
            double max = double.NegativeInfinity;

            int nRow = doubleMatrix.GetLength(0);
            int nCol = doubleMatrix.GetLength(1);

            double[,] normalizedMatrix = new double[nRow, nCol];

            // find the maximum and minimum value
            for (int i = 0; i < nRow; i++)
            {
                for (int j = 0; j < nCol; j++)
                {
                    if (doubleMatrix[i, j] < min)
                        min = doubleMatrix[i, j];
                    if (doubleMatrix[i, j] > max)
                        max = doubleMatrix[i, j];
                }
            }

            // compute the affine normalization
            for (int i = 0; i < nRow; i++)
            {
                for (int j = 0; j < nCol; j++)
                {
                    normalizedMatrix[i, j] = Math.Max(((doubleMatrix[i, j] - min) / (max - min)), Constants.eps);
                }
            }
            return normalizedMatrix;
        }

        /// <summary>
        /// Affine normalizazion for float[,] matrix. Used in ILnumerics form
        /// </summary>
        /// <param name="matrix">Matrix as float[,]</param>
        /// <returns>Affine normalized matrix as float[,]</returns>
        public static float[,] AffineNormalizeMatrix(float[,] floatMatrix)
        {
            float min = float.PositiveInfinity;
            float max = float.NegativeInfinity;

            int nRow = floatMatrix.GetLength(0);
            int nCol = floatMatrix.GetLength(1);

            float[,] normalizedMatrix = new float[nRow, nCol];

            // find the maximum and minimum value
            for (int i = 0; i < nRow; i++)
            {
                for (int j = 0; j < nCol; j++)
                {
                    if (floatMatrix[i, j] < min)
                        min = floatMatrix[i, j];
                    if (floatMatrix[i, j] > max)
                        max = floatMatrix[i, j];
                }
            }

            // compute the affine normalization
            for (int i = 0; i < nRow; i++)
            {
                for (int j = 0; j < nCol; j++)
                {
                    normalizedMatrix[i, j] = ((floatMatrix[i, j] - min) / (max - min)) + (float)Math.Pow(2, -23);
                }
            }
            return normalizedMatrix;
        }

        /// <summary>
        /// Get the row 'i' of matrix  
        /// </summary>
        /// <param name="i">row index</param>
        /// <returns>row in Matrix type</returns>
        public Matrix GetRow(int i)
        {
            int nCol = matrix.GetLength(1);
            double[,] outM = new double[1, nCol];
            Matrix outMatrix = new Matrix(1, nCol);

            for (int j = 0; j < nCol; j++)
            {
                outM[0, j] = matrix[i, j];
            }
            
            outMatrix.Value = outM;
            return outMatrix;
        }

        /// <summary>
        /// Get the Column 'j' of matrix
        /// </summary>
        /// <param name="j">column index</param>
        /// <returns>column in Matrix type</returns>
        public Matrix GetColumn(int j)
        {
            int nRow = matrix.GetLength(0);
            double[,] outM = new double[nRow, 1];
            Matrix outMatrix = new Matrix(nRow, 1);

            for (int i = 0; i < nRow; i++)
            {
                outM[i, 0] = matrix[i, j];
            }

            outMatrix.Value = outM;
            return outMatrix;
        }

        /// <summary>
        /// Set the row 'i' of matrix 
        /// </summary>
        /// <param name="i">row index</param>
        /// <param name="row">Row to set</param>
        public void SetRow(int i, Matrix row)
        {
            int nCol = matrix.GetLength(1);

            for (int j = 0; j < nCol; j++)
            {
                matrix[i, j] = row.Value[0, j];
            }
        }

        /// <summary>
        /// Set the column 'j' of matrix
        /// </summary>
        /// <param name="j">column index</param>
        /// <param name="column">Column to set</param>
        public void SetCol(int j, Matrix column)
        {
            int nRow = matrix.GetLength(0);

            for (int i = 0; i < nRow; i++)
            {
                matrix[i, j] = column.Value[i, 0];
            }
        }

        /// <summary>
        /// Affine normalization of Matrix
        /// </summary>
        /// <returns>Matrix normalized</returns>
        public Matrix AffineNormalization()
        {
            Matrix outMatrix;

            double min = double.PositiveInfinity;
            double max = double.NegativeInfinity;
            
            int nRow = matrix.GetLength(0);
            int nCol = matrix.GetLength(1);

            outMatrix = new Matrix(nRow, nCol);

            // find the maximum and minimum value
            for (int i = 0; i < nRow; i++)
            {
                for (int j = 0; j < nCol; j++)
                {                   
                    if (matrix[i, j] < min)
                        min = matrix[i, j];
                    if (matrix[i, j] > max)
                        max = matrix[i, j];
                }
            }

            // compute the affine normalization
            for (int i = 0; i < nRow; i++)
            {
                for (int j = 0; j < nCol; j++)
                {
                    outMatrix.Value[i,j] = Math.Max(((matrix[i, j] - min) / (max - min)), Constants.eps);
                }
            }

            return outMatrix;
        }

        /// <summary>
        /// Linear normalization for matrix
        /// </summary>
        /// <returns>Linear normalized matrix as Matrix type</returns>
        public Matrix LinearNormalization()
        {
            Matrix outMatrix;

            double max = double.NegativeInfinity;

            int nRow = matrix.GetLength(0);
            int nCol = matrix.GetLength(1);

            outMatrix = new Matrix(nRow, nCol);

            // find the maximum value
            for (int i = 0; i < nRow; i++)
            {
                for (int j = 0; j < nCol; j++)
                {
                    if (matrix[i, j] > max)
                        max = matrix[i, j];
                }
            }

            // compute the linear normalization
            for (int i = 0; i < nRow; i++)
            {
                for (int j = 0; j < nCol; j++)
                {
                    outMatrix.Value[i, j] = Math.Max((matrix[i, j] / max), Constants.eps);
                }
            }

            return outMatrix;
        }

        /// <summary>
        /// Transpose matrix
        /// </summary>
        /// <returns>Transposed matrix as Matrix type</returns>
        public Matrix GetTransposed()
        {
            Matrix transposedMatrix = null;

            int nRow = matrix.GetLength(0);
            int nCol = matrix.GetLength(1);

            transposedMatrix = new Matrix(nCol, nRow);

            for (int i = 0; i < nRow; i++)
            {
                for (int j = 0; j < nCol; j++)
                {
                    transposedMatrix.Value[j, i] = matrix[i, j];
                }
            }

            return transposedMatrix;
        }

        /// <summary>
        /// Compute the median filter of length filterLength on matrix
        /// </summary>
        /// <param name="filterLength">Length of median filter</param>
        /// <returns>Filtered matrix</returns>
        public Matrix GetMedianFilter(int filterLength)
        {
            // fifo to realize window
            Queue<double> fifo = new Queue<double>(filterLength);

            // Extract dimensions
            int nRows = this.NumberOfRows;
            int nCols = this.NumberOfCol;

            // Set bounds and out index
            int leftBound = (int)Math.Ceiling((double)(filterLength / 2));
            int rightBound = filterLength / 2;
            int outIndex = filterLength / 2;

            Matrix mediateMatrix = new Matrix(nRows, nCols);

            // Apply median filter per row
            for (int a = 0; a < nRows; a++)
            {
                // Inizialize fifo on leftBound with value equal to array[0]
                for (int w = 0; w < filterLength; w++)
                {
                    if (w < leftBound)
                    {
                        fifo.Enqueue(this.Value[a, 0]);
                    }
                    else
                    {
                        fifo.Enqueue(this.Value[a, w - leftBound + 1]);
                    }
                }

                for (int hop = 0; hop < nCols; hop++)
                {
                    // List used to order value
                    List<double> orderedFifo;

                    // Add element in fifo and delete the last one
                    if (hop + rightBound + 1 < nCols)
                    {
                        fifo.Enqueue(this.Value[a, hop + rightBound + 1]);
                    }
                    else
                    {
                        fifo.Enqueue(this.Value[a, nCols - 1]);
                    }
                    fifo.Dequeue();

                    // order elements
                    orderedFifo = new List<double>(fifo.OrderBy(x => x));

                    // Extract the middle element
                    mediateMatrix.Value[a, hop] = orderedFifo.ElementAt(outIndex);
                }
                // Clear the fifo before next row
                fifo.Clear();
            }
            return mediateMatrix;
        }

        /// <summary>
        /// Derivate matrix with finite difference method and normalize it
        /// </summary>
        /// <returns>Derivate matrix as Matrix type</returns>
        public Matrix GetNormalizedDerivate()
        {
            // Extract dimensions
            int nRows = this.NumberOfRows;
            int nCols = this.NumberOfCol;

            Matrix derivate = new Matrix(nRows, nCols);
            Matrix column = new Matrix(nRows, 1);
            Matrix oldColumn = this.GetColumn(0);

            for (int hop = 0; hop < nCols; hop++)
            {
                column = this.GetColumn(hop);
                derivate.SetCol(hop, (column - oldColumn));
                oldColumn = column;
            }
            // Set the negative value to 0.0
            derivate = derivate.FilterNegativeValue();
            // Normalize
            derivate = derivate.LinearNormalization();
            return derivate;
        }

        /// <summary>
        /// Set to 0 the negative values. Set the lower bound to 0.
        /// </summary>
        /// <returns>filtered Matrix</returns>
        public Matrix FilterNegativeValue()
        {
            Matrix outMatrix;

            int nRow = matrix.GetLength(0);
            int nCol = matrix.GetLength(1);

            outMatrix = new Matrix(nRow, nCol);

            for (int i = 0; i < nRow; i++)
            {
                for (int j = 0; j < nCol; j++)
                {
                    if (matrix[i, j] < 0)
                    {
                        outMatrix.Value[i, j] = 0.0;
                    }
                    else
                    {
                        outMatrix.Value[i, j] = matrix[i, j];
                    }
                }
            }
            return outMatrix;
        }

        public Matrix GetSign()
        {
            Matrix signMatrix = null;

            int nRow = matrix.GetLength(0);
            int nCol = matrix.GetLength(1);

            signMatrix = new Matrix(nRow, nCol);

            for (int i = 0; i < nRow; i++)
            {
                for (int j = 0; j < nCol; j++)
                {
                    signMatrix.Value[i, j] = Math.Sign(matrix[i, j]);
                }
            }

            return signMatrix;
        }

        /// <summary>
        /// Define matrix multiplication row*column
        /// </summary>
        /// <param name="matrixA">Left factor</param>
        /// <param name="matrixB">Right factor</param>
        /// <returns>Matrix product between A and B</returns>
        public static Matrix operator *(Matrix matrixA, Matrix matrixB)
        {
            Matrix outMatrix = null;

            int rowsA = matrixA.NumberOfRows;
            int colsA = matrixA.NumberOfCol;

            int rowsB = matrixB.NumberOfRows;
            int colsB = matrixB.NumberOfCol;

            if (colsA == rowsB)
            {
                outMatrix = new Matrix(rowsA, colsB);
                int r = colsA;

                for (int i = 0; i < rowsA; i++)
                {
                    for (int j = 0; j < colsB; j++)
                    {
                        double sum = 0;
                        for (int k = 0; k < r; k++)
                        {
                            sum += matrixA.Value[i, k] * matrixB.Value[k, j];
                        }
                        outMatrix.Value[i, j] = sum + Constants.eps;
                    }
                }

                return outMatrix;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Define scalar * Matrix
        /// </summary>
        /// <param name="alfa">scalar factor</param>
        /// <param name="matrixA">Matrix factor</param>
        /// <returns>scalar * matrix</returns>
        public static Matrix operator *(double alfa, Matrix matrixA)
        {
            int rowsA = matrixA.NumberOfRows;
            int colsA = matrixA.NumberOfCol;

            Matrix outMatrix = new Matrix(rowsA, colsA);

            for (int i = 0; i < rowsA; i++)
            {
                for (int j = 0; j < colsA; j++)
                {
                    outMatrix.Value[i,j] = alfa * matrixA.Value[i,j];
                }
            }

            return outMatrix;
        }

        /// <summary>
        /// Define scalar * Matrix
        /// </summary>
        /// <param name="alfa">Scalar factor</param>
        /// <param name="matrixA">Matrix Factor</param>
        /// <returns>Scalar * Matrix</returns>
        public static Matrix operator *(Matrix matrixA, double alfa)
        {
            int rowsA = matrixA.NumberOfRows;
            int colsA = matrixA.NumberOfCol;

            Matrix outMatrix = new Matrix(rowsA, colsA);

            for (int i = 0; i < rowsA; i++)
            {
                for (int j = 0; j < colsA; j++)
                {
                    outMatrix.Value[i, j] = alfa * matrixA.Value[i, j];
                }
            }

            return outMatrix;
        }
        
        /// <summary>
        /// Division member by member
        /// </summary>
        /// <param name="matrixA">Matrix A</param>
        /// <param name="matrixB">Matrix B</param>
        /// <returns>MatrixA / MatrixB</returns>
        public static Matrix operator / (Matrix matrixA, Matrix matrixB)
        {
            Matrix outMatrix = null;

            if ((matrixA.NumberOfRows == matrixB.NumberOfRows) && (matrixA.NumberOfCol == matrixB.NumberOfCol))
            {
                int nRow = matrixA.NumberOfRows;
                int nCol = matrixA.NumberOfCol;

                outMatrix = new Matrix(nRow, nCol);

                for (int i = 0; i < nRow; i++)
                {
                    for (int j = 0; j < nCol; j++)
                    {
                        outMatrix.Value[i, j] = matrixA.Value[i, j] / matrixB.Value[i, j];
                    }
                }             
            }

            return outMatrix;
        }

        /// <summary>
        /// Multiplication member by member
        /// </summary>
        /// <param name="matrixA">Matrix A</param>
        /// <param name="matrixB">Matrix B</param>
        /// <returns>MatrixA .X MatrixB</returns>
        public static double[,] MultiplicateMemberWise(double[,] matrixA, double[,] matrixB)
        {
            double[,] outMatrix = null;

            if ((matrixA.GetLength(0) == matrixB.GetLength(0)) && (matrixA.GetLength(1) == matrixB.GetLength(1)))
            {
                int nRow = matrixA.GetLength(0);
                int nCol = matrixA.GetLength(1);

                outMatrix = new double[nRow, nCol];

                for (int i = 0; i < nRow; i++)
                {
                    for (int j = 0; j < nCol; j++)
                    {
                        outMatrix[i, j] = matrixA[i, j] * matrixB[i, j];
                    }
                }
            }
            return outMatrix;
        }

        /// <summary>
        /// Subtract matrix
        /// </summary>
        /// <param name="mtxA">Matrix A</param>
        /// <param name="mtxB">Matrix B</param>
        /// <returns>A - B</returns>
        public static Matrix operator -(Matrix matrixA, Matrix matrixB)
        {
            Matrix outMatrix = null;

            if ((matrixA.NumberOfRows == matrixB.NumberOfRows) && (matrixA.NumberOfCol == matrixB.NumberOfCol))
            {
                int nRow = matrixA.NumberOfRows;
                int nCol = matrixA.NumberOfCol;

                outMatrix = new Matrix(nRow, nCol);

                for (int i = 0; i < nRow; i++)
                {
                    for (int j = 0; j < nCol; j++)
                    {
                        outMatrix.Value[i, j] = matrixA.Value[i, j] - matrixB.Value[i, j];
                    }
                }
            }

            return outMatrix;
        }

        /// <summary>
        /// Add matrix
        /// </summary>
        /// <param name="mtxA">Matrix A</param>
        /// <param name="mtxB">Matrix B</param>
        /// <returns>A + B</returns>
        public static Matrix operator +(Matrix matrixA, Matrix matrixB)
        {
            Matrix outMatrix = null;

            if ((matrixA.NumberOfRows == matrixB.NumberOfRows) && (matrixA.NumberOfCol == matrixB.NumberOfCol))
            {
                int nRow = matrixA.NumberOfRows;
                int nCol = matrixA.NumberOfCol;

                outMatrix = new Matrix(nRow, nCol);

                for (int i = 0; i < nRow; i++)
                {
                    for (int j = 0; j < nCol; j++)
                    {
                        outMatrix.Value[i, j] = matrixA.Value[i, j] + matrixB.Value[i, j];
                    }
                }
            }

            return outMatrix;
        }

        #endregion
    }
}
