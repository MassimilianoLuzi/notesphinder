﻿// Copyright (c) 2015, 2016 Massimiliano Luzi, Mario Antonelli and Antonello Rizzi
// University of Rome "La Sapienza"
//
// massimiliano.luzi@uniroma1.it
// mario.antonelli@gmail.com 
// antonello.rizzi@uniroma1.it 
//
// This file is part of NotesPHInder.
//
// NotesPHInder is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// NotesPHInder is distributed in the hope that it will be useful. 
// IT IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
// PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
// CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with NotesPHInder.If not, see<http://www.gnu.org/licenses/>.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Windows.Forms;

namespace NotesPHInder
{
    /// <summary>
    /// Class for handling midi files
    /// </summary>
    class MidiHandler
    {
        #region Global Variables

        // Set the midi command code
        static readonly byte noteOnCommand = 9;
        static readonly byte noteOffCommand = 8;
        
        /// <summary>
        /// Struct for definition of note event
        /// </summary>
        struct MidiNoteEvent
        {
            public byte[] deltaTimes;
            public byte eventType_midiChannel;
            public byte noteNumber;
            public byte noteVelocity;

            public MidiNoteEvent(byte[] dTime, byte eventType, byte channel, byte number, byte velocity)
            {
                deltaTimes = dTime;
                eventType_midiChannel = (byte)(eventType << 4 | channel);
                noteNumber = number;
                noteVelocity = velocity;                
            }

        }

        /// <summary>
        /// Struct for definition of Header Chunk
        /// </summary>
        struct HeaderChunk
        {
            public string chunkID;
            public int chunkSize;
            public ushort formatType;
            public ushort numberOfTrack;
            public ushort timeDivision;

            public HeaderChunk(ushort fType, ushort nTrack, ushort tDivision)
            {
                chunkID = "MThd";
                chunkSize = 6;
                formatType = fType;
                numberOfTrack = nTrack;
                timeDivision = tDivision;
            }
        }

        /// <summary>
        /// Struct for definition of Track Chunk
        /// </summary>
        struct TrackChunk
        {
            public string chunkID;
            public int chunkSize;
            public MidiNoteEvent[] eventTrack;
            //public byte[] endTrack;

            public TrackChunk(MidiNoteEvent[] eventArray)
            {
                chunkID = "MTrk";

                int nByte = eventArray.Length * 3;
                for (int i = 0; i < eventArray.Length; i++)
                {
                    nByte += eventArray[i].deltaTimes.Length;
                }               
                chunkSize = nByte/* + 7*/;
                eventTrack = eventArray;
                            
            }
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Compute variable length encoding of data
        /// </summary>
        /// <param name="data">Value to encoding </param>
        /// <returns>Encoded value</returns>
        private byte[] VariableLengthEncoding(int data)
        {
            List<byte> outList = new List<byte>();
            int value = data;
            byte lower7bits;
            bool first = true;
            while (first || value > 0)
            {
                if (first)
                {
                    lower7bits = (byte)(value & 0x7f);
                    outList.Add(lower7bits);
                    first = false;
                }
                else
                {                                       
                    lower7bits = (byte)(value | 128);
                    outList.Add(lower7bits);                    
                }
                value >>= 7;
            }
            return outList.ToArray();
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor. Do nothing
        /// </summary>.
        public MidiHandler()
        {       
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Create a midi file from onset and offset matrix
        /// </summary>
        /// <param name="onsetMatrix">onset matrix</param>
        /// <param name="offsetMatrix">offset matrix</param>
        /// <param name="notesPlayed">mini notes code of notes played</param>
        /// <param name="type">midi type</param>
        /// <param name="numberOfTrack">number of track in midi</param>
        /// <param name="channel">midi channel</param>
        /// <param name="timeDivision">time resolution for delta times</param>
        /// <param name="path">out path of midi file</param>
        public void CreateMidiFile(Matrix onsetMatrix, 
            Matrix offsetMatrix, 
            int[] notesPlayed, 
            ushort type, 
            ushort numberOfTrack,
            byte channel, 
            ushort timeDivision, string path)
        {
            HeaderChunk header = new HeaderChunk(type, numberOfTrack, timeDivision);
            MidiNoteEvent noteEvent = new MidiNoteEvent();

            List<byte> byteToWrite = new List<byte>();

            List<MidiNoteEvent> notesEventList = new List<MidiNoteEvent>();
            
            int numberOfNotes = onsetMatrix.NumberOfRows;
            int numberOfHops = onsetMatrix.NumberOfCol; 

            byte noteVelocity = 100;
            int deltaTime = 0;
            for (int hop = 0; hop < numberOfHops; hop++)
            {
                for (int a = 0; a < numberOfNotes; a++)
                {
                    if (onsetMatrix.Value[a, hop] != 0)
                    {
                        byte velocity = (byte)(onsetMatrix.Value[a, hop] * 127);
                        noteEvent = new MidiNoteEvent(VariableLengthEncoding(deltaTime), noteOnCommand, channel, (byte)notesPlayed[a], noteVelocity);
                        notesEventList.Add(noteEvent);
                        deltaTime = 0;
                    }

                    else if (offsetMatrix.Value[a, hop] == -1)
                    {
                        noteEvent = new MidiNoteEvent(VariableLengthEncoding(deltaTime), noteOffCommand, channel, (byte)notesPlayed[a], noteVelocity);
                        notesEventList.Add(noteEvent);
                        deltaTime = 0;
                    }
                }
                deltaTime++;
            }
            // End Of Track
            noteEvent = new MidiNoteEvent(VariableLengthEncoding(deltaTime),0xFF ,0xFF ,47, 0);
            notesEventList.Add(noteEvent);

            // Generate array of event from List
            MidiNoteEvent[] eventArray = notesEventList.ToArray();
            notesEventList.Clear();

            // generate Track
            TrackChunk trackChunk = new TrackChunk(eventArray);

            // header ID
            char[] headerID = header.chunkID.ToCharArray();
            byteToWrite.Add((byte)headerID[0]);
            byteToWrite.Add((byte)headerID[1]);
            byteToWrite.Add((byte)headerID[2]);
            byteToWrite.Add((byte)headerID[3]);

            // header side
            byteToWrite.Add((byte)(header.chunkSize >> 24 & 0x000000ff));
            byteToWrite.Add((byte)(header.chunkSize >> 16 & 0x000000ff));
            byteToWrite.Add((byte)(header.chunkSize >> 8 & 0x000000ff));
            byteToWrite.Add((byte)(header.chunkSize & 0x000000ff));

            // header type
            byteToWrite.Add((byte)(header.formatType >> 8 & 0x00ff));
            byteToWrite.Add((byte)(header.formatType & 0x00ff));

            // header n Track
            byteToWrite.Add((byte)(header.numberOfTrack >> 8 & 0x00ff));
            byteToWrite.Add((byte)(header.numberOfTrack & 0x00ff));

            // header time division
            byteToWrite.Add((byte)(header.timeDivision >> 8 & 0x00ff));
            byteToWrite.Add((byte)(header.timeDivision & 0x00ff));

            // track id
            char[] trackID = trackChunk.chunkID.ToCharArray();
            byteToWrite.Add((byte)trackID[0]);
            byteToWrite.Add((byte)trackID[1]);
            byteToWrite.Add((byte)trackID[2]);
            byteToWrite.Add((byte)trackID[3]);

            // track size
            byteToWrite.Add((byte)(trackChunk.chunkSize >> 24 & 0x000000ff));
            byteToWrite.Add((byte)(trackChunk.chunkSize >> 16 & 0x000000ff));
            byteToWrite.Add((byte)(trackChunk.chunkSize >> 8 & 0x000000ff));
            byteToWrite.Add((byte)(trackChunk.chunkSize & 0x000000ff));

            // midi event
            for (int i = 0; i < eventArray.Length; i++)
            {
                // delta times
                for(int j=eventArray[i].deltaTimes.Length-1; j>=0; j--)
                {
                    byteToWrite.Add(eventArray[i].deltaTimes[j]);
                }

                byteToWrite.Add(eventArray[i].eventType_midiChannel);
                byteToWrite.Add(eventArray[i].noteNumber);
                byteToWrite.Add(eventArray[i].noteVelocity);
            }

            byte[] byteArray = byteToWrite.ToArray();
            

            if (path != null)
            {
                FileStream fw = new FileStream(path, FileMode.Create);
                using (BinaryWriter bw = new BinaryWriter(fw))
                {
                    for (int i = 0; i < byteArray.Length; i++)
                    {
                        bw.Write(byteArray[i]);
                    }
                }
            }
            

        }

        #endregion
    }
}
