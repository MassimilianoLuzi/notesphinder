﻿// Copyright (c) 2015, 2016 Massimiliano Luzi, Mario Antonelli and Antonello Rizzi
// University of Rome "La Sapienza"
//
// massimiliano.luzi@uniroma1.it
// mario.antonelli@gmail.com 
// antonello.rizzi@uniroma1.it 
//
// This file is part of NotesPHInder.
//
// NotesPHInder is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// NotesPHInder is distributed in the hope that it will be useful. 
// IT IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
// PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
// CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with NotesPHInder.If not, see<http://www.gnu.org/licenses/>.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NotesPHInder
{
    /// <summary>
    /// Optimization function for transcription
    /// </summary>
    public static class OptimizationFunction
    {
        /// <summary>
        /// Definition of struct used for otpimization results
        /// </summary>
        public struct outMinimization
        {
            /// <summary>
            /// cost function value
            /// </summary>
            public double costFunctionValue;
            /// <summary>
            /// Out vector of otpimization
            /// </summary>
            public Matrix vectorX;

            /// <summary>
            /// Struct constructor
            /// </summary>
            /// <param name="p_costFunctionValue">cost function</param>
            /// <param name="p_vectorX">out of optimization</param>
            public outMinimization(double p_costFunctionValue, Matrix p_vectorX)
            {
                costFunctionValue = p_costFunctionValue;
                vectorX = p_vectorX;
            }
        }

        /// <summary>
        /// Projected gradient method with quadratic cost function
        /// </summary>
        /// <param name="vectorY">Approximating vector</param>
        /// <param name="notesDictionary">Note dictionary used for approximation</param>
        /// <returns>Struct with cost function value and out of approximation</returns>
        public static outMinimization MinimizeQuadraticFunction(Matrix vectorY, Matrix notesDictionary)
        {
            // Extract dimensions
            int numberOfFrequencies = notesDictionary.NumberOfRows;
            int numberOfNotes = notesDictionary.NumberOfCol;

            // Parameter for Gradient method 
            int nMaxIteration = 100;
            double threshold = Math.Pow(10, -5);
            double lambda = Math.Pow(10, -6);
            double esse;
            int consecutiveTrue = 0;

            // Cost function variables
            double costFunction = 0;
            double oldCostFunction = 0;

            // Generate the Identity Matrix
            Matrix identiy = new Matrix(numberOfNotes, numberOfNotes);
            for (int i = 0; i < numberOfNotes; i++)
            {
                identiy.Value[i, i] = 1.0;
            }

            // Matrix Variables for optimization
            Matrix gradient = new Matrix(numberOfNotes, 1);
            Matrix oldGradient = new Matrix(numberOfNotes, 1);
            Matrix projectionVector = new Matrix(numberOfNotes, 1);
            Matrix direction = new Matrix(numberOfNotes, 1);
            Matrix noteDictionaryTransposed = notesDictionary.GetTransposed();

            // Queue for the Armijo rule
            Queue<double> fifo = new Queue<double>();
            int fifoLenght = 5;

            // Inizialize vectorX with random numbers.
            Matrix vectorX = new Matrix(numberOfNotes, 1);
            System.Random random = new Random();
            for (int a = 0; a < numberOfNotes; a++)
            {
                vectorX.Value[a, 0] = random.NextDouble();
            }
            Matrix oldVectorX = vectorX;

            // Begin Iteration
            for (int nIter = 0; nIter < numberOfNotes * nMaxIteration; nIter++)
            {
                // compute the AX multiplication
                Matrix matrixAX = notesDictionary * vectorX;

                // calulate gradient
                gradient = (noteDictionaryTransposed * (matrixAX - vectorY)) + lambda * vectorX;

                // Barzilai Borwein method
                Matrix barzilaiDelta = vectorX - oldVectorX;
                Matrix barzilaiGamma = gradient - oldGradient;
                Matrix num;
                Matrix den;

                // Utilize alternatively the two value for esse
                if (nIter % 2 == 0)
                {
                    num = (barzilaiGamma.GetTransposed() * barzilaiGamma);
                    den = (barzilaiDelta.GetTransposed() * barzilaiGamma);
                }
                else
                {
                    num = (barzilaiDelta.GetTransposed() * barzilaiGamma);
                    den = (barzilaiDelta.GetTransposed() * barzilaiDelta);
                }

                esse = Math.Pow(num.Value[0, 0] / den.Value[0, 0], -1);

                oldGradient = gradient;
                oldVectorX = vectorX;

                // Project gradient to be non negative
                projectionVector = vectorX - (esse * gradient);
                for (int a = 0; a < numberOfNotes; a++)
                {
                    if (projectionVector.Value[a, 0] < 0)
                    {
                        projectionVector.Value[a, 0] = 0.0;
                    }
                }
                direction = projectionVector - vectorX;

                // Armijo rule
                double gamma = Math.Pow(10, -4);
                double delta = 0.5;
                double alfa = 1.0;

                // Compute oldFunc
                oldCostFunction = 0.0;
                for (int f = 0; f < numberOfFrequencies; f++)
                {
                    oldCostFunction += Math.Pow(vectorY.Value[f, 0] - matrixAX.Value[f, 0], 2);
                }

                for (int a = 0; a < numberOfNotes; a++)
                {
                    oldCostFunction += lambda * Math.Pow(vectorX.Value[a, 0], 2);
                    //oldCostFunction += lambda * Math.Abs(vectorX.Value[a, 0]);
                }
                oldCostFunction /= 2;

                // Add oldFunc to queue of lenght 5
                fifo.Enqueue(oldCostFunction);
                if (fifo.Count >= fifoLenght)
                {
                    fifo.Dequeue();
                }

                double referenceValue = fifo.Max();
                while (true)
                {
                    // Compute the new vectorX with the current value of alfa and compute the cost function
                    Matrix tempVectorX = vectorX + (alfa * direction);
                    Matrix tempMatrixAX = notesDictionary * tempVectorX;

                    // Calulate func
                    double tempFunc = 0.0;
                    for (int f = 0; f < numberOfFrequencies; f++)
                    {
                        tempFunc += Math.Pow(vectorY.Value[f, 0] - tempMatrixAX.Value[f, 0], 2);
                    }

                    for (int a = 0; a < numberOfNotes; a++)
                    {
                        tempFunc += lambda * Math.Pow(tempVectorX.Value[a, 0], 2);
                        //tempFunc += lambda * Math.Abs(tempVectorX.Value[a, 0]);
                    }
                    tempFunc /= 2;

                    // If is verified the condition set the vectorX, func and stop, else alfa = alfa*delta
                    if (tempFunc <= referenceValue + (gamma * alfa) * ((gradient.GetTransposed() * direction).Value[0, 0]))
                    {
                        vectorX = tempVectorX;
                        costFunction = tempFunc;
                        break;
                    }
                    else
                    {
                        alfa = alfa * delta;
                    }
                }

                // Verify the stop condition:
                // 1. calculate the difference between func and oldFunc
                // 2. stop if delta < threshold 10 times consecutively

                double difference = Math.Abs(costFunction - oldCostFunction);

                if (difference < threshold)
                {
                    consecutiveTrue++;

                    if (consecutiveTrue > 10)
                    {
                        break;
                    }
                }
                else
                {
                    consecutiveTrue = 0;
                }
                oldCostFunction = costFunction;
            }
            outMinimization outResult = new outMinimization();
            outResult.costFunctionValue = costFunction;
            outResult.vectorX = vectorX;

            return outResult;
        }

        /// <summary>
        /// Projected gradient method with divergence cost function
        /// </summary>
        /// <param name="vectorY">Approximating vector</param>
        /// <param name="notesDictionary">Note dictionary used for approximation</param>
        /// <returns>Struct with cost function value and out of approximation</returns>
        public static outMinimization MinimizeDivergenceFunction(Matrix vectorY, Matrix notesDictionary)
        {
            // Extract dimensions
            int numberOfFrequencies = notesDictionary.NumberOfRows;
            int numberOfNotes = notesDictionary.NumberOfCol;

            // Parameter for Gradient method 
            int nMaxIteration = 100;
            double threshold = Math.Pow(10, -5);
            double lambda = Math.Pow(10, -6);
            double esse;
            int consecutiveTrue = 0;

            // Cost function variables
            double costFunction = 0;
            double oldCostFunction = 0;

            // Generate the Identity Matrix
            Matrix identiy = new Matrix(numberOfNotes, numberOfNotes);
            for (int i = 0; i < numberOfNotes; i++)
            {
                identiy.Value[i, i] = 1.0;
            }

            // Matrix Variables for optimization
            Matrix gradient = new Matrix(numberOfNotes, 1);
            Matrix oldGradient = new Matrix(numberOfNotes, 1);
            Matrix projectionVector = new Matrix(numberOfNotes, 1);
            Matrix direction = new Matrix(numberOfNotes, 1);
            Matrix noteDictionaryTransposed = notesDictionary.GetTransposed();

            // Queue for the Armijo rule
            Queue<double> fifo = new Queue<double>();
            int fifoLenght = 5;

            Matrix additiveVector = new Matrix(numberOfNotes, 1);
            // Calculate Additive Vector
            for (int a = 0; a < numberOfNotes; a++)
            {
                for (int f = 0; f < numberOfFrequencies; f++)
                {
                    additiveVector.Value[a, 0] += notesDictionary.Value[f, a];
                }
            }

            // Inizialize vectorX with random numbers.
            Matrix vectorX = new Matrix(numberOfNotes, 1);
            System.Random random = new Random();
            for (int a = 0; a < numberOfNotes; a++)
            {
                vectorX.Value[a, 0] = random.NextDouble();
            }
            Matrix oldVectorX = vectorX;

            // Begin Iteration
            for (int nIter = 0; nIter < numberOfNotes * nMaxIteration; nIter++)
            {
                // compute the AX multiplication
                Matrix matrixAX = notesDictionary * vectorX;

                // calulate gradient
                gradient = additiveVector - noteDictionaryTransposed * (vectorY / matrixAX) + lambda * vectorX;

                // Barzilai Borwein method
                Matrix barzilaiDelta = vectorX - oldVectorX;
                Matrix barzilaiGamma = gradient - oldGradient;
                Matrix num;
                Matrix den;

                // Utilize alternatively the two value for esse
                if (nIter % 2 == 0)
                {
                    num = (barzilaiGamma.GetTransposed() * barzilaiGamma);
                    den = (barzilaiDelta.GetTransposed() * barzilaiGamma);
                }
                else
                {
                    num = (barzilaiDelta.GetTransposed() * barzilaiGamma);
                    den = (barzilaiDelta.GetTransposed() * barzilaiDelta);
                }

                esse = Math.Pow(num.Value[0, 0] / den.Value[0, 0], -1);

                oldGradient = gradient;
                oldVectorX = vectorX;

                // Project gradient to be non negative
                projectionVector = vectorX - (esse * gradient);
                for (int a = 0; a < numberOfNotes; a++)
                {
                    if (projectionVector.Value[a, 0] < 0)
                    {
                        projectionVector.Value[a, 0] = 0.0;
                    }
                }
                direction = projectionVector - vectorX;

                // Armijo rule
                double gamma = Math.Pow(10, -4);
                double delta = 0.5;
                double alfa = 1.0;

                // Compute oldFunc
                oldCostFunction = 0.0;
                for (int f = 0; f < numberOfFrequencies; f++)
                {
                    oldCostFunction += (vectorY.Value[f, 0] * Math.Log(vectorY.Value[f, 0] / (matrixAX.Value[f, 0]))) - vectorY.Value[f, 0] + matrixAX.Value[f, 0];
                }

                for (int a = 0; a < numberOfNotes; a++)
                {
                    oldCostFunction += (lambda * Math.Pow(vectorX.Value[a, 0], 2)) / 2;
                    //oldCostFunction += (lambda * Math.Abs(vectorX.Value[a, 0])) / 2;
                }

                // Add oldFunc to queue of lenght 5
                fifo.Enqueue(oldCostFunction);
                if (fifo.Count >= fifoLenght)
                {
                    fifo.Dequeue();
                }

                double referenceValue = fifo.Max();
                while (true)
                {
                    // Compute the new vectorX with the current value of alfa and compute the cost function
                    Matrix tempVectorX = vectorX + (alfa * direction);
                    Matrix tempMatrixAX = notesDictionary * tempVectorX;

                    // Calulate func
                    double tempFunc = 0.0;
                    for (int f = 0; f < numberOfFrequencies; f++)
                    {
                        tempFunc += (vectorY.Value[f, 0] * Math.Log(vectorY.Value[f, 0] / (tempMatrixAX.Value[f, 0]))) - vectorY.Value[f, 0] + tempMatrixAX.Value[f, 0];
                    }
                    for (int a = 0; a < numberOfNotes; a++)
                    {
                        tempFunc += (lambda * Math.Pow(vectorX.Value[a, 0], 2)) / 2;
                        //tempFunc += (lambda * Math.Abs(vectorX.Value[a, 0])) / 2;
                    }

                    // If is verified the condition set the vectorX, func and stop, else alfa = alfa*delta
                    if (tempFunc <= referenceValue + (gamma * alfa) * ((gradient.GetTransposed() * direction).Value[0, 0]))
                    {
                        vectorX = tempVectorX;
                        costFunction = tempFunc;
                        break;
                    }
                    else
                    {
                        alfa = alfa * delta;
                    }
                }

                // Verify the stop condition:
                // 1. calculate the difference between func and oldFunc
                // 2. stop if delta < threshold 10 times consecutively

                double difference = Math.Abs(costFunction - oldCostFunction);

                if (difference < threshold)
                {
                    consecutiveTrue++;

                    if (consecutiveTrue > 10)
                    {
                        break;
                    }
                }
                else
                {
                    consecutiveTrue = 0;
                }
                oldCostFunction = costFunction;
            }

            outMinimization outResult = new outMinimization();
            outResult.costFunctionValue = costFunction;
            outResult.vectorX = vectorX;

            return outResult;
        }

        /// <summary>
        /// Viterbi algoritm for HMM postprocessing
        /// </summary>
        /// <param name="noteActivity">temporal activity of note</param>
        /// <param name="transitionMatrix">Transition matrix of the HMM</param>
        /// <param name="initialState">Initial state: note off = 0; note on = 1)</param>
        /// <returns>activity of note</returns>
        public static double[] ViterbiHmm(double[] noteActivity, double[,] transitionMatrix, int initialState)
        {
            // Set parameter
            double C = 75;
            double offset = 0.18;

            int length = noteActivity.Length;
            double[,] emission = new double[2, length];

            // Set the emission probability
            for (int i = 0; i < length; i++)
            {
                double sigmoid = 1.0 / (1 + Math.Exp(-(noteActivity[i] - offset)*C));
                
                emission[0, i] = 1 - sigmoid;
                emission[1, i] = sigmoid;
            }
            
            double[] currentState = new double[length];
            double[,] pTransition = new double[2, length];
            double[] v = new double[2];
            if (initialState == 0)
            {
                v[1] = double.NegativeInfinity;
            }
            else
            {
                v[0] = double.NegativeInfinity;
            }
            double[] vOld = v;

            for (int count = 0; count < length; count++)
            {
                for (int state = 0; state < 2; state++)
                {
                    double bestValue = double.NegativeInfinity;
                    double bestPTR = 0;

                    for (int inner = 0; inner < 2; inner++)
                    {
                        double val = vOld[inner] + Math.Log(transitionMatrix[inner, state]);
                        if (val > bestValue)
                        {
                            bestValue = val;
                            bestPTR = inner;
                        }
                    }

                    pTransition[state, count] = bestPTR;
                    v[state] = Math.Log(emission[state, count]) + bestValue;
                }
                vOld = v;
            }

            double finalState = 0;

            double max = double.NegativeInfinity;
            for (int i = 0; i < 2; i++)
            {
                double value = v[i];
                if (value > max)
                {
                    finalState = i;
                    max = value;
                }
            }

            currentState[length-1] = finalState; 
            for (int count = length - 2; count >= 0; count--)
            {
                currentState[count] = pTransition[(int)currentState[count+1], count+1];   
            }
            return currentState;
        }
    }
}
