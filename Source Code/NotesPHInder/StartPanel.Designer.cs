﻿// Copyright (c) 2015, 2016 Massimiliano Luzi, Mario Antonelli and Antonello Rizzi
// University of Rome "La Sapienza"
//
// massimiliano.luzi@uniroma1.it
// mario.antonelli@gmail.com 
// antonello.rizzi@uniroma1.it 
//
// This file is part of NotesPHInder.
//
// NotesPHInder is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// NotesPHInder is distributed in the hope that it will be useful. 
// IT IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
// PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
// CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with NotesPHInder.If not, see<http://www.gnu.org/licenses/>.

namespace NotesPHInder
{
    partial class StartPanel
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(StartPanel));
            this.startAsyncButton = new System.Windows.Forms.Button();
            this.CqtWorker = new System.ComponentModel.BackgroundWorker();
            this.cancelAsyncButton = new System.Windows.Forms.Button();
            this.openWaveFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.buttonOpen = new System.Windows.Forms.Button();
            this.waveLabel = new System.Windows.Forms.Label();
            this.ClassifierWorker = new System.ComponentModel.BackgroundWorker();
            this.WavePlayButton = new System.Windows.Forms.Button();
            this.TranscriptionWorker = new System.ComponentModel.BackgroundWorker();
            this.label1 = new System.Windows.Forms.Label();
            this.midiPlayButton = new System.Windows.Forms.Button();
            this.showSpectrumButton = new System.Windows.Forms.Button();
            this.showActivityButton = new System.Windows.Forms.Button();
            this.selectDictionary = new System.Windows.Forms.ComboBox();
            this.midiLabel = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.selectCompression = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.trainingButton = new System.Windows.Forms.Button();
            this.localProgressBar = new NotesPHInder.CustomProgressBar();
            this.SuspendLayout();
            // 
            // startAsyncButton
            // 
            this.startAsyncButton.Location = new System.Drawing.Point(12, 11);
            this.startAsyncButton.Name = "startAsyncButton";
            this.startAsyncButton.Size = new System.Drawing.Size(165, 23);
            this.startAsyncButton.TabIndex = 0;
            this.startAsyncButton.Text = "Transcribe";
            this.startAsyncButton.UseVisualStyleBackColor = true;
            this.startAsyncButton.Click += new System.EventHandler(this.startAsyncButton_Click);
            // 
            // CqtWorker
            // 
            this.CqtWorker.DoWork += new System.ComponentModel.DoWorkEventHandler(this.CqtWorker_DoWork);
            this.CqtWorker.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.CqtWorker_ProgressChanged);
            this.CqtWorker.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.CqtWorker_RunWorkerCompleted);
            // 
            // cancelAsyncButton
            // 
            this.cancelAsyncButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cancelAsyncButton.Location = new System.Drawing.Point(196, 11);
            this.cancelAsyncButton.Name = "cancelAsyncButton";
            this.cancelAsyncButton.Size = new System.Drawing.Size(165, 23);
            this.cancelAsyncButton.TabIndex = 5;
            this.cancelAsyncButton.Text = "Cancel";
            this.cancelAsyncButton.UseVisualStyleBackColor = true;
            this.cancelAsyncButton.Click += new System.EventHandler(this.cancelAsyncButton_Click);
            // 
            // buttonOpen
            // 
            this.buttonOpen.Location = new System.Drawing.Point(12, 100);
            this.buttonOpen.Name = "buttonOpen";
            this.buttonOpen.Size = new System.Drawing.Size(165, 40);
            this.buttonOpen.TabIndex = 6;
            this.buttonOpen.TabStop = false;
            this.buttonOpen.Text = "Open Wav";
            this.buttonOpen.UseVisualStyleBackColor = true;
            this.buttonOpen.Click += new System.EventHandler(this.buttonOpen_Click);
            // 
            // waveLabel
            // 
            this.waveLabel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.waveLabel.AutoEllipsis = true;
            this.waveLabel.Location = new System.Drawing.Point(12, 227);
            this.waveLabel.Name = "waveLabel";
            this.waveLabel.Size = new System.Drawing.Size(165, 18);
            this.waveLabel.TabIndex = 0;
            this.waveLabel.Text = "Wave:";
            this.waveLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // WavePlayButton
            // 
            this.WavePlayButton.Location = new System.Drawing.Point(12, 69);
            this.WavePlayButton.Name = "WavePlayButton";
            this.WavePlayButton.Size = new System.Drawing.Size(165, 25);
            this.WavePlayButton.TabIndex = 9;
            this.WavePlayButton.Text = "Play Wave";
            this.WavePlayButton.UseVisualStyleBackColor = true;
            this.WavePlayButton.Click += new System.EventHandler(this.Play_Click);
            // 
            // TranscriptionWorker
            // 
            this.TranscriptionWorker.DoWork += new System.ComponentModel.DoWorkEventHandler(this.TranscriptionWorker_DoWork);
            this.TranscriptionWorker.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.TranscriptionWorker_ProgressChanged);
            this.TranscriptionWorker.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.TranscriptionWorker_RunWorkerCompleted);
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(148, 98);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(0, 13);
            this.label1.TabIndex = 8;
            this.label1.Visible = false;
            // 
            // midiPlayButton
            // 
            this.midiPlayButton.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.midiPlayButton.Location = new System.Drawing.Point(196, 69);
            this.midiPlayButton.Name = "midiPlayButton";
            this.midiPlayButton.Size = new System.Drawing.Size(165, 25);
            this.midiPlayButton.TabIndex = 12;
            this.midiPlayButton.Text = "Play Midi";
            this.midiPlayButton.UseVisualStyleBackColor = true;
            this.midiPlayButton.Click += new System.EventHandler(this.MidiPlayButton_Click);
            // 
            // showSpectrumButton
            // 
            this.showSpectrumButton.Location = new System.Drawing.Point(281, 100);
            this.showSpectrumButton.Name = "showSpectrumButton";
            this.showSpectrumButton.Size = new System.Drawing.Size(80, 40);
            this.showSpectrumButton.TabIndex = 13;
            this.showSpectrumButton.Text = "Show Spectrum";
            this.showSpectrumButton.UseVisualStyleBackColor = true;
            this.showSpectrumButton.Click += new System.EventHandler(this.ShowSpectrumButton_Click);
            // 
            // showActivityButton
            // 
            this.showActivityButton.Location = new System.Drawing.Point(196, 100);
            this.showActivityButton.Name = "showActivityButton";
            this.showActivityButton.Size = new System.Drawing.Size(80, 40);
            this.showActivityButton.TabIndex = 14;
            this.showActivityButton.Text = "Show Notes Activity";
            this.showActivityButton.UseVisualStyleBackColor = true;
            this.showActivityButton.Click += new System.EventHandler(this.ShowActivityButton_Click);
            // 
            // selectDictionary
            // 
            this.selectDictionary.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.selectDictionary.FormattingEnabled = true;
            this.selectDictionary.Location = new System.Drawing.Point(196, 199);
            this.selectDictionary.Name = "selectDictionary";
            this.selectDictionary.Size = new System.Drawing.Size(165, 21);
            this.selectDictionary.TabIndex = 7;
            // 
            // midiLabel
            // 
            this.midiLabel.AutoEllipsis = true;
            this.midiLabel.Location = new System.Drawing.Point(196, 227);
            this.midiLabel.Name = "midiLabel";
            this.midiLabel.Size = new System.Drawing.Size(165, 18);
            this.midiLabel.TabIndex = 15;
            this.midiLabel.Text = "Midi: ";
            this.midiLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 202);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(106, 13);
            this.label3.TabIndex = 19;
            this.label3.Text = "Instrument Dictionary";
            // 
            // selectCompression
            // 
            this.selectCompression.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.selectCompression.FormattingEnabled = true;
            this.selectCompression.Location = new System.Drawing.Point(196, 172);
            this.selectCompression.Name = "selectCompression";
            this.selectCompression.Size = new System.Drawing.Size(165, 21);
            this.selectCompression.TabIndex = 20;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 175);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(171, 13);
            this.label4.TabIndex = 21;
            this.label4.Text = "High Dynamic Range Compression";
            // 
            // trainingButton
            // 
            this.trainingButton.Location = new System.Drawing.Point(12, 146);
            this.trainingButton.Name = "trainingButton";
            this.trainingButton.Size = new System.Drawing.Size(349, 23);
            this.trainingButton.TabIndex = 23;
            this.trainingButton.Text = "Create Dictionary";
            this.trainingButton.UseVisualStyleBackColor = true;
            this.trainingButton.Click += new System.EventHandler(this.trainingButton_Click);
            // 
            // localProgressBar
            // 
            this.localProgressBar.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.localProgressBar.CustomText = "Open a wave file";
            this.localProgressBar.DisplayStyle = NotesPHInder.ProgressBarDisplayText.CustomText;
            this.localProgressBar.Location = new System.Drawing.Point(12, 40);
            this.localProgressBar.Name = "localProgressBar";
            this.localProgressBar.Size = new System.Drawing.Size(349, 23);
            this.localProgressBar.TabIndex = 22;
            this.localProgressBar.Click += new System.EventHandler(this.localProgressBar_Click);
            // 
            // StartPanel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(373, 255);
            this.Controls.Add(this.trainingButton);
            this.Controls.Add(this.localProgressBar);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.selectCompression);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.midiLabel);
            this.Controls.Add(this.showActivityButton);
            this.Controls.Add(this.showSpectrumButton);
            this.Controls.Add(this.midiPlayButton);
            this.Controls.Add(this.WavePlayButton);
            this.Controls.Add(this.waveLabel);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.selectDictionary);
            this.Controls.Add(this.buttonOpen);
            this.Controls.Add(this.cancelAsyncButton);
            this.Controls.Add(this.startAsyncButton);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "StartPanel";
            this.Text = "Notes PHInder";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button startAsyncButton;
        private System.ComponentModel.BackgroundWorker CqtWorker;
        private System.Windows.Forms.Button cancelAsyncButton;
        private System.Windows.Forms.OpenFileDialog openWaveFileDialog;
        private System.Windows.Forms.Button buttonOpen;
        private System.Windows.Forms.Label waveLabel;
        private System.ComponentModel.BackgroundWorker ClassifierWorker;
        private System.Windows.Forms.Button WavePlayButton;
        private System.ComponentModel.BackgroundWorker TranscriptionWorker;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button midiPlayButton;
        private System.Windows.Forms.Button showSpectrumButton;
        private System.Windows.Forms.Button showActivityButton;
        private System.Windows.Forms.Label midiLabel;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox selectCompression;
        private System.Windows.Forms.Label label4;
        private CustomProgressBar localProgressBar;
        private System.Windows.Forms.Button trainingButton;
        public System.Windows.Forms.ComboBox selectDictionary;
    }
}

