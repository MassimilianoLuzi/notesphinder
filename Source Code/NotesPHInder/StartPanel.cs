﻿// Copyright (c) 2015, 2016 Massimiliano Luzi, Mario Antonelli and Antonello Rizzi
// University of Rome "La Sapienza"
//
// massimiliano.luzi@uniroma1.it
// mario.antonelli@gmail.com 
// antonello.rizzi@uniroma1.it 
//
// This file is part of NotesPHInder.
//
// NotesPHInder is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// NotesPHInder is distributed in the hope that it will be useful. 
// IT IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
// PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
// CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with NotesPHInder.If not, see<http://www.gnu.org/licenses/>.

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Media;
using System.Threading;
using System.Threading.Tasks;

namespace NotesPHInder
{
    /// <summary>
    /// Main form
    /// </summary>
    public partial class StartPanel : Form
    {
        #region Global Variables

        // Variables for wave handling
        string wavePath = null;
        WaveHandler wave = null;

        // Variables for CQT computation
        CQT cqt = null;
        double hopTime = 0.01;
        double toneSpacing;
        double[,] normalizedCqt = null;
        int hopSize;
        
        // Variable for transcription
        Matrix temporalActivityMatrix = null;
        Matrix onsetMatrix = null;
        Matrix offsetMatrix = null;
        int[] midiNotes = new int[72];
        string midiPath = null;
        string dictionary = null;
        
        // Boolean flags
        bool cancelled;
        bool postProcessingHMMFlag;
        bool compressionFlag;

        /// <summary>
        /// struct for storing results
        /// </summary>
        struct ResultStruct
        {
            public Matrix temporalActivityMatrix;
            public Matrix onsetMatrix;
            public Matrix offsetMatrix;
            public int[] midiNotesPlayed;

            public ResultStruct(Matrix tempActivity, Matrix onset, Matrix offset, int[] array)
            {
                temporalActivityMatrix = tempActivity;
                onsetMatrix = onset;
                offsetMatrix = offset;
                midiNotesPlayed = array;
            }
        }

        // Variables for graphics
        CqtImage cqtImage = null;
        TemporalActivityImage temporalActivityImage = null;
        // Workaround ILnumerics bug
        ILnumericsGhost ghostForm = null;

        #endregion
        
        #region Private Methods
        
        /// <summary>
        /// Start button handler
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void startAsyncButton_Click(System.Object sender,
            System.EventArgs e)
        {
            if (wave != null)
            {   
                // Read flags for pre and post processing
                compressionFlag = bool.Parse(selectCompression.SelectedValue.ToString());

                // Read dictionary type
                dictionary = selectDictionary.Text;

                // Extract sample rate of samples
                int sampleRate = wave.SampleRate;

                // Compute the space between tones
                toneSpacing = Math.Pow(2, 1.0 / Constants.binsPerOctave);

                // Compute Q value
                int Q = (int)Math.Ceiling(1.0 / (toneSpacing - 1));

                // Set the Max Window Size and then the max freqeunce and the number of frequences
                //int maxWindowSize = sampleRate / 10;
                int maxWindowSize = (int)Math.Floor((sampleRate * Q) / Constants.minFrequence);
                int maxFrequence = (int)Math.Floor(sampleRate / 2.0);                
                int numFrequences = (int)Math.Floor(Math.Log(maxFrequence / Constants.minFrequence) / Math.Log(toneSpacing));

                // Read number of frequencies in dictionary.
                int numFreqInDictionary;
                //string dictionaryPath = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Automatic Music Transcription\\Dictionaries\\" + dictionary + ".dat";
                string dictionaryPath = "Data\\Dictionaries\\" + dictionary + ".dat";
                using (StreamReader reader = new StreamReader(dictionaryPath))
                {
                    // Read the dimension of matrix
                    int numberA = int.Parse(reader.ReadLine());
                    numFreqInDictionary = int.Parse(reader.ReadLine());
                }

                if (numFrequences != numFreqInDictionary)
                {
                    MessageBox.Show("Sample Rate of dictionary is different from wave file Sample Rate", "Error");
                    wave = null;
                }
                else
                {
                    // Inizialize cqt class
                    cqt = new CQT(sampleRate, numFrequences, maxWindowSize, Q, toneSpacing);

                    // Set button status
                    this.buttonOpen.Enabled = false;
                    this.midiPlayButton.Enabled = false;
                    this.selectCompression.Enabled = false;
                    this.selectDictionary.Enabled = false;
                    this.showActivityButton.Enabled = false;
                    this.showSpectrumButton.Enabled = false;
                    this.startAsyncButton.Enabled = false;
                    this.cancelAsyncButton.Enabled = true;
                    cancelled = false;

                    // Start cqt computation
                    CqtWorker.RunWorkerAsync();
                }
            }
            else
            {
                MessageBox.Show("Select a wave file");
            }
        }

        /// <summary>
        /// Cancel button handler
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cancelAsyncButton_Click(System.Object sender,
            System.EventArgs e)
        {
            // Cancel the asynchronous operation.
            this.CqtWorker.CancelAsync();
            this.TranscriptionWorker.CancelAsync();

            // Set the enable state of buttons
            startAsyncButton.Enabled = true;
            buttonOpen.Enabled = true;
            cancelAsyncButton.Enabled = false;
            midiPlayButton.Enabled = false;
            selectCompression.Enabled = true;
            selectDictionary.Enabled = true;
            showSpectrumButton.Enabled = false;
            showSpectrumButton.Enabled = false;

            // Set cancel flag
            cancelled = true;
        }

        /// <summary>
        /// Open button handler
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonOpen_Click(object sender, EventArgs e)
        {
            OpenFileDialog openWaveDialog = new OpenFileDialog();

            openWaveDialog.Filter = "wave files (*.wav)|*.wav|All files (*.*)|*.*";
            openWaveDialog.FilterIndex = 1;

            openWaveDialog.InitialDirectory = @"Data\Samples\";
            openWaveDialog.RestoreDirectory = true;
            openWaveDialog.AutoUpgradeEnabled = false;

            if (openWaveDialog.ShowDialog() == DialogResult.OK)
            {
                wavePath = openWaveDialog.FileName;
                waveLabel.Text = "Wave: " + Path.GetFileName(wavePath);
                wave = new WaveHandler(wavePath);
            }

            startAsyncButton.Enabled = true;
            cancelAsyncButton.Enabled = false;
        }

        /// <summary>
        /// Play Button for wav file 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Play_Click(object sender, EventArgs e)
        {

            if (wavePath != null && wave != null)
            {
                System.Diagnostics.Process.Start(wavePath);
            }
            else
            {
                MessageBox.Show("Select a wave file");
            }
        }

        /// <summary>
        /// Play Button for midi file
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MidiPlayButton_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start(midiPath);
        }

        /// <summary>
        /// Show temporal activity graphic
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ShowActivityButton_Click(object sender, EventArgs e)
        {
            if (temporalActivityMatrix != null)
            {
                temporalActivityImage = new TemporalActivityImage(temporalActivityMatrix.Value, midiNotes);
                temporalActivityImage.Show();
            }

        }

        /// <summary>
        /// Show CQT graphics
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ShowSpectrumButton_Click(object sender, EventArgs e)
        {
            if (normalizedCqt != null)
            {
                cqtImage = new CqtImage(normalizedCqt, toneSpacing, Constants.binsPerOctave);
                cqtImage.Show();
            }
        }

        /// <summary>
        /// Open Training form
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void trainingButton_Click(object sender, EventArgs e)
        {
            TrainingPanel trainingPanel = new TrainingPanel();
            trainingPanel.Show(this);
        }

        /// <summary>
        /// Click Action for local progress bar
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void localProgressBar_Click(object sender, EventArgs e)
        {
            // Start open file dialog by click progress bar
            if (localProgressBar.CustomText == "Open a wave file" || localProgressBar.CustomText == "Cancelled")
            {
                OpenFileDialog openWaveDialog = new OpenFileDialog();

                openWaveDialog.InitialDirectory = @"Data\Samples\";
                openWaveDialog.RestoreDirectory = true;
                openWaveDialog.AutoUpgradeEnabled = false;

                openWaveDialog.Filter = "wave files (*.wav)|*.wav|All files (*.*)|*.*";
                openWaveDialog.FilterIndex = 1;

                startAsyncButton.Enabled = true;
                cancelAsyncButton.Enabled = false;
                if (openWaveDialog.ShowDialog() == DialogResult.OK)
                {
                    wavePath = openWaveDialog.FileName;
                    waveLabel.Text = "Wave: " + Path.GetFileName(wavePath);
                    wave = new WaveHandler(wavePath);
                }
            }

            // Save midi by click Progress Bar
            if (localProgressBar.CustomText == "Done. Click here to save midi")
            {
                // Create MIDI file
                MidiHandler midi = new MidiHandler();
                SaveFileDialog saveMidiDialog = new SaveFileDialog();
                saveMidiDialog.Filter = "midi file (*.mid)|*.mid";
                saveMidiDialog.AddExtension = true;
                saveMidiDialog.InitialDirectory = @"Outputs\";
                saveMidiDialog.AutoUpgradeEnabled = false;
                saveMidiDialog.FileName = "out_" + Path.GetFileNameWithoutExtension(wavePath);
                if (saveMidiDialog.ShowDialog() == DialogResult.OK)
                {
                    midiPath = saveMidiDialog.FileName;
                    midiLabel.Text = "Midi: " + Path.GetFileName(midiPath);

                    ushort tickPerBeat = (ushort)Math.Round((0.5 / hopSize) * wave.SampleRate);

                    midi.CreateMidiFile(onsetMatrix,
                        offsetMatrix,
                        midiNotes,
                        0, 1, 0, tickPerBeat, midiPath);

                    midiPlayButton.Enabled = true;
                }
            }
        }

        #region Cqt worker

        /// <summary>
        /// Constant Q Transform: run work
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CqtWorker_DoWork(object sender,
            DoWorkEventArgs e)
        {
            // Extract the samples from wave and normalize then
            short[] samples = wave.ExtractMonoSample();
            double[] doubleSamples = wave.GetDoubleSamples(samples);
            this.localProgressBar.DisplayStyle = ProgressBarDisplayText.CustomText;

            // Apply Dynamic Compression
            double[] normalizedSamples = wave.GetLinearNormalizedSamples(doubleSamples);
            if (compressionFlag)
            {
                this.localProgressBar.CustomText = "Pre-Processing: Dynamic Compression";
                double rmsValue_dB = wave.GetRsmDbValue(normalizedSamples);
                double compThreshold = rmsValue_dB;
                normalizedSamples = wave.GetCompressionFilter(normalizedSamples, compThreshold, 60, 1, 100, 10, wave.SampleRate, CqtWorker);
                normalizedSamples = wave.GetLinearNormalizedSamples(normalizedSamples);
            }

            // Compute the cqt transform
            this.localProgressBar.CustomText = "Evaluate Constant Q Transform";
            hopSize = (int)Math.Round(wave.SampleRate * hopTime);
            double[,] cqtTransorfm = cqt.GetTranform(normalizedSamples, hopSize, CqtWorker);

            // Store the result for the runworker complete method
            e.Result = cqtTransorfm;
        }

        /// <summary>
        /// Constant Q Transform: Work completed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CqtWorker_RunWorkerCompleted(
            object sender, RunWorkerCompletedEventArgs e)
        {           
            // First, handle the case where an exception was thrown.
            if (cancelled)
            {
                this.localProgressBar.CustomText = "Cancelled";
                this.localProgressBar.Value = 0;
                this.localProgressBar.Refresh();
            }
            else
            {
                // Finally, handle the case where the operation 
                // succeeded.
                double[,] cqtTransform = (double[,])e.Result;
                normalizedCqt = Matrix.AffineNormalizeMatrix(cqtTransform);

                // Start transcription
                TranscriptionWorker.RunWorkerAsync();
            }         
        }

        /// <summary>
        /// Constant Q Transform Progress Bar Update
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CqtWorker_ProgressChanged(object sender,
            ProgressChangedEventArgs e)
        {
            this.localProgressBar.Value = e.ProgressPercentage; 
        }

        #endregion

        #region Trancription worker

        /// <summary>
        /// Transcription work
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TranscriptionWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            // Label for Progress Bar
            this.localProgressBar.CustomText = "Transcription Process";

            // Declare onset and offset matirx
            Matrix onset = null;
            Matrix offset = null;

            // Extract dimensions
            int numberOfFrequences = normalizedCqt.GetLength(0);
            int numberOfHops = normalizedCqt.GetLength(1);

            // Catch the dictionary path
            string dictionaryPath = "Data\\Dictionaries\\" + dictionary + ".dat";
            
            // Create recognition class
            TemporalRecognition recognition = new TemporalRecognition(normalizedCqt, dictionaryPath);

            // Declare variable struct for store results
            TemporalRecognition.temporalResultStruct temporalResult = new TemporalRecognition.temporalResultStruct();

            // Compute Transcription
            temporalResult = recognition.TranscriptionMethod(hopSize, wave.SampleRate, TranscriptionWorker);
            
            // Extract temporal activity
            temporalActivityMatrix = temporalResult.temporalActivity;
                
            // Normalize and aplly median filter
            temporalActivityMatrix = temporalActivityMatrix.LinearNormalization();
            int medianWindowLength = (int)Math.Round(0.12 / hopTime);
            temporalActivityMatrix = temporalActivityMatrix.GetMedianFilter(medianWindowLength);
            temporalActivityMatrix = temporalActivityMatrix.LinearNormalization();

            if (!cancelled)
            {
                // Post Processing

                // Set Progress bar label
                this.localProgressBar.CustomText = "Post-Processing: Hidden Markov Model";
                // HMM Post Processing
                Matrix midiActivity = recognition.GetMidiActivityMatrix(temporalActivityMatrix, TranscriptionWorker);
                onset = recognition.GetOnsetMatrixHmm(midiActivity);
                offset = recognition.GetOffsetMatrixHmm(midiActivity);
                
            }

            // Export Results
            ResultStruct result = new ResultStruct(temporalActivityMatrix, onset, offset, midiNotes);
            e.Result = result;
        }

        /// <summary>
        /// Transcription progress bar update
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TranscriptionWorker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {   
            localProgressBar.Value = e.ProgressPercentage;
        }

        /// <summary>
        /// Transcription Complete
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TranscriptionWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (cancelled)
            {
                localProgressBar.CustomText = "Cancelled";
                localProgressBar.Value = 0;
                localProgressBar.Refresh();
            }
            else
            {
                localProgressBar.CustomText = "Done. Click here to save midi";
                localProgressBar.Value = 0;
                localProgressBar.Refresh();

                // Change buttons state
                this.startAsyncButton.Enabled = true;
                this.cancelAsyncButton.Enabled = false;
                this.buttonOpen.Enabled = true;
                this.selectDictionary.Enabled = true;
                this.selectCompression.Enabled = true;
                this.showSpectrumButton.Enabled = true;
                this.showActivityButton.Enabled = true;

                // Extract results
                ResultStruct resultStruct = (ResultStruct)e.Result;
                temporalActivityMatrix = resultStruct.temporalActivityMatrix;
                onsetMatrix = resultStruct.onsetMatrix;
                offsetMatrix = resultStruct.offsetMatrix;

                // Create MIDI file
                MidiHandler midi = new MidiHandler();
                SaveFileDialog saveMidiDialog = new SaveFileDialog();
                saveMidiDialog.Filter = "midi file (*.mid)|*.mid";
                saveMidiDialog.AddExtension = true;
                saveMidiDialog.InitialDirectory = @"Outputs\";
                saveMidiDialog.AutoUpgradeEnabled = false;
                saveMidiDialog.FileName = "out_" + Path.GetFileNameWithoutExtension(wavePath);
                if (saveMidiDialog.ShowDialog() == DialogResult.OK)
                {
                    midiPath = saveMidiDialog.FileName;
                    midiLabel.Text = "Midi: " + Path.GetFileName(midiPath);

                    ushort tickPerBeat = (ushort)Math.Round((0.5 / hopSize) * wave.SampleRate);

                    midi.CreateMidiFile(resultStruct.onsetMatrix,
                        resultStruct.offsetMatrix,
                        midiNotes,
                        0, 1, 0, tickPerBeat, midiPath);

                    midiPlayButton.Enabled = true;
                }
            }
        }

        #endregion

        #endregion

        #region Constructor

        /// <summary>
        /// Construnctor: Inizialize all items
        /// </summary>
        public StartPanel()
        {
            InitializeComponent();

            // Set backgroundworker parameter
            CqtWorker.WorkerReportsProgress = true;
            CqtWorker.WorkerSupportsCancellation = true;
            TranscriptionWorker.WorkerReportsProgress = true;
            TranscriptionWorker.WorkerSupportsCancellation = true;

            // Inizialize cancelled flag
            cancelled = false;

            // Set enabled buttons
            startAsyncButton.Enabled = true;
            cancelAsyncButton.Enabled = false;
            midiPlayButton.Enabled = false;
            showActivityButton.Enabled = false;
            showSpectrumButton.Enabled = false;

            // Inizialize midi notes array
            for (int n = 0; n < 72; n++)
            {
                midiNotes[n] = n + 24;
            }

            // Inizialize Compression Combo Box
            selectCompression.DisplayMember = "Text";
            selectCompression.ValueMember = "Value";

            var itemsComp = new[] {
                new { Text = "Enabled", Value = "true" },
                new { Text = "Disabled", Value = "false" },
            };

            selectCompression.DataSource = itemsComp;
            selectCompression.SelectedIndex = 0;

            // Inizialize dictionary Combo Box
            //string[] itemsDictionary = Directory.GetFiles(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Automatic Music Transcription\\Dictionaries\\");
            string[] itemsDictionary = Directory.GetFiles(@"Data\Dictionaries\", "*.dat");

            for (int s = 0; s < itemsDictionary.Length; s++)
            {
                itemsDictionary[s] = Path.GetFileNameWithoutExtension(itemsDictionary[s]);
            }

            selectDictionary.Items.AddRange(itemsDictionary);
            selectDictionary.SelectedIndex = selectDictionary.Items.IndexOf("Generic");

            // Open a ghost form as a workaround for Ilnumerics bug
            ghostForm = new ILnumericsGhost();
            
            ghostForm.Show();
            ghostForm.Visible = false;
        }

        #endregion       
    }
}
