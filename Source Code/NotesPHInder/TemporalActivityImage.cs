﻿// Copyright (c) 2015, 2016 Massimiliano Luzi, Mario Antonelli and Antonello Rizzi
// University of Rome "La Sapienza"
//
// massimiliano.luzi@uniroma1.it
// mario.antonelli@gmail.com 
// antonello.rizzi@uniroma1.it 
//
// This file is part of NotesPHInder.
//
// NotesPHInder is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// NotesPHInder is distributed in the hope that it will be useful. 
// IT IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
// PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
// CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with NotesPHInder.If not, see<http://www.gnu.org/licenses/>.

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ILNumerics;
using ILNumerics.Drawing;
using ILNumerics.Drawing.Plotting;

namespace NotesPHInder
{
    /// <summary>
    /// Class for visualization of Temporal Activity
    /// </summary>
    public partial class TemporalActivityImage : Form
    {
        // Global Variables
        float[][] freqMatrix;
        float[][] tempMatrix;
        int[] midiNotesPlayed;

        /// <summary>
        /// Constructor: Initialize Data
        /// </summary>
        /// <param name="tempM"></param>
        /// <param name="midiNotes"></param>
        public TemporalActivityImage(double[,] tempM, int[] midiNotes)
        {
            InitializeComponent();

            int dimensionA = tempM.GetLength(0);
            int numberOfTimes = tempM.GetLength(1);
            freqMatrix = new float[dimensionA][];
            tempMatrix = new float[dimensionA][];
            midiNotesPlayed = midiNotes;
            
            // Convert double matrix to float matrix
            for (int a = 0; a < dimensionA; a++)
            {
                tempMatrix[a] = new float[numberOfTimes];

                for (int t = 0; t < numberOfTimes; t++)
                {                   
                    tempMatrix[a][t] = (float)tempM[a, t];
                }
            }
        }

        /// <summary>
        /// Create Graphic
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ilPanel1_Load(object sender, EventArgs e)
        {
            int dimensionA = tempMatrix.Length;
            var scene = new ILScene();
            
            // Set axes label
            var tempPlot = scene.Add(new ILPlotCube());
            tempPlot.Axes.XAxis.Label.Text = "Time";
            tempPlot.Axes.YAxis.Label.Text = "Notes";

            ILTick tick;
            int counter = 0;

            for (int a = 0; a < dimensionA; a++)
            {
                ILArray<float> tempArray = tempMatrix[a];
                int octave = (midiNotesPlayed[a] / 12) - 1;

                if (IsReducedView.Checked)
                {
                    
                    if (tempArray.Max() > Math.Pow(2, -23))
                    {
                        tempPlot.Add
                            (
                                new ILLinePlot(tempArray.T + counter)
                            );

                        // Set Y ticks
                        switch (midiNotesPlayed[a] % 12)
                        {
                            case 0:
                                tick = tempPlot.Axes.YAxis.Ticks.Add(counter, "C" + octave.ToString());
                                tick.Label.Anchor = new PointF(1f, 0.5f);
                                break;

                            case 1:
                                tick = tempPlot.Axes.YAxis.Ticks.Add(counter, "C#" + octave.ToString());
                                tick.Label.Anchor = new PointF(1f, 0.5f);
                                break;

                            case 2:
                                tick = tempPlot.Axes.YAxis.Ticks.Add(counter, "D" + octave.ToString());
                                tick.Label.Anchor = new PointF(1f, 0.5f);
                                break;

                            case 3:
                                tick = tempPlot.Axes.YAxis.Ticks.Add(counter, "D#" + octave.ToString());
                                tick.Label.Anchor = new PointF(1f, 0.5f);
                                break;

                            case 4:
                                tick = tempPlot.Axes.YAxis.Ticks.Add(counter, "E" + octave.ToString());
                                tick.Label.Anchor = new PointF(1f, 0.5f);
                                break;

                            case 5:
                                tick = tempPlot.Axes.YAxis.Ticks.Add(counter, "F" + octave.ToString());
                                tick.Label.Anchor = new PointF(1f, 0.5f);
                                break;

                            case 6:
                                tick = tempPlot.Axes.YAxis.Ticks.Add(counter, "F#" + octave.ToString());
                                tick.Label.Anchor = new PointF(1f, 0.5f);
                                break;

                            case 7:
                                tick = tempPlot.Axes.YAxis.Ticks.Add(counter, "G" + octave.ToString());
                                tick.Label.Anchor = new PointF(1f, 0.5f);
                                break;

                            case 8:
                                tick = tempPlot.Axes.YAxis.Ticks.Add(counter, "G#" + octave.ToString());
                                tick.Label.Anchor = new PointF(1f, 0.5f);
                                break;

                            case 9:
                                tick = tempPlot.Axes.YAxis.Ticks.Add(counter, "A" + octave.ToString());
                                tick.Label.Anchor = new PointF(1f, 0.5f);
                                break;

                            case 10:
                                tick = tempPlot.Axes.YAxis.Ticks.Add(counter, "A#" + octave.ToString());
                                tick.Label.Anchor = new PointF(1f, 0.5f);
                                break;

                            case 11:
                                tick = tempPlot.Axes.YAxis.Ticks.Add(counter, "B" + octave.ToString());
                                tick.Label.Anchor = new PointF(1f, 0.5f);
                                break;

                            default:
                                break;
                        }
                        counter++;
                    }
                }
                else
                {
                    tempPlot.Add
                            (
                                new ILLinePlot(tempArray.T + a)
                            );

                    switch (midiNotesPlayed[a] % 12)
                    {
                        case 0:
                            tick = tempPlot.Axes.YAxis.Ticks.Add(a, "C" + octave.ToString());
                            tick.Label.Anchor = new PointF(1f, 0.5f);
                            tick.Label.Font = new Font(FontFamily.GenericSansSerif, 6);
                            break;

                        case 1:
                            tick = tempPlot.Axes.YAxis.Ticks.Add(a, "C#" + octave.ToString());
                            tick.Label.Anchor = new PointF(1f, 0.5f);
                            tick.Label.Font = new Font(FontFamily.GenericSansSerif, 6);
                            break;

                        case 2:
                            tick = tempPlot.Axes.YAxis.Ticks.Add(a, "D" + octave.ToString());
                            tick.Label.Anchor = new PointF(1f, 0.5f);
                            tick.Label.Font = new Font(FontFamily.GenericSansSerif, 6);
                            break;

                        case 3:
                            tick = tempPlot.Axes.YAxis.Ticks.Add(a, "D#" + octave.ToString());
                            tick.Label.Anchor = new PointF(1f, 0.5f);
                            tick.Label.Font = new Font(FontFamily.GenericSansSerif, 6);
                            break;

                        case 4:
                            tick = tempPlot.Axes.YAxis.Ticks.Add(a, "E" + octave.ToString());
                            tick.Label.Anchor = new PointF(1f, 0.5f);
                            tick.Label.Font = new Font(FontFamily.GenericSansSerif, 6);
                            break;

                        case 5:
                            tick = tempPlot.Axes.YAxis.Ticks.Add(a, "F" + octave.ToString());
                            tick.Label.Anchor = new PointF(1f, 0.5f);
                            tick.Label.Font = new Font(FontFamily.GenericSansSerif, 6);
                            break;

                        case 6:
                            tick = tempPlot.Axes.YAxis.Ticks.Add(a, "F#" + octave.ToString());
                            tick.Label.Anchor = new PointF(1f, 0.5f);
                            tick.Label.Font = new Font(FontFamily.GenericSansSerif, 6);
                            break;

                        case 7:
                            tick = tempPlot.Axes.YAxis.Ticks.Add(a, "G" + octave.ToString());
                            tick.Label.Anchor = new PointF(1f, 0.5f);
                            tick.Label.Font = new Font(FontFamily.GenericSansSerif, 6);
                            break;

                        case 8:
                            tick = tempPlot.Axes.YAxis.Ticks.Add(a, "G#" + octave.ToString());
                            tick.Label.Anchor = new PointF(1f, 0.5f);
                            tick.Label.Font = new Font(FontFamily.GenericSansSerif, 6);
                            break;

                        case 9:
                            tick = tempPlot.Axes.YAxis.Ticks.Add(a, "A" + octave.ToString());
                            tick.Label.Anchor = new PointF(1f, 0.5f);
                            tick.Label.Font = new Font(FontFamily.GenericSansSerif, 6);
                            break;

                        case 10:
                            tick = tempPlot.Axes.YAxis.Ticks.Add(a, "A#" + octave.ToString());
                            tick.Label.Anchor = new PointF(1f, 0.5f);
                            tick.Label.Font = new Font(FontFamily.GenericSansSerif, 6);
                            break;

                        case 11:
                            tick = tempPlot.Axes.YAxis.Ticks.Add(a, "B" + octave.ToString());
                            tick.Label.Anchor = new PointF(1f, 0.5f);
                            tick.Label.Font = new Font(FontFamily.GenericSansSerif, 6);
                            break;

                        default:
                            break;
                    }
                }
                
            }

            ilPanel1.Scene = scene;
            ilPanel1.Refresh();
        }
    }
}
