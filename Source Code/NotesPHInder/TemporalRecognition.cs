﻿// Copyright (c) 2015, 2016 Massimiliano Luzi, Mario Antonelli and Antonello Rizzi
// University of Rome "La Sapienza"
//
// massimiliano.luzi@uniroma1.it
// mario.antonelli@gmail.com 
// antonello.rizzi@uniroma1.it 
//
// This file is part of NotesPHInder.
//
// NotesPHInder is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// NotesPHInder is distributed in the hope that it will be useful. 
// IT IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
// PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
// CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with NotesPHInder.If not, see<http://www.gnu.org/licenses/>.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.ComponentModel;
using System.Threading;
using System.Threading.Tasks;

namespace NotesPHInder
{
    /// <summary>
    /// Class for transcription task
    /// </summary>
    class TemporalRecognition
    {
        #region Global Variables

        // Dictionary variables
        Matrix notesDictionaryMatrix = null;
        Matrix allNotesDictionaryMatrix = null;

        // temporal activity matrix
        Matrix temporalActivityMatrix = null;

        // Temporal activity column: input of optimization
        Matrix matrixY = null;

        // Spectrogram variable
        double[,] cqtTransform;
        
        // array for storing cost function 
        double[] costFunctionValue;

        // List of notes for reducing base
        List<int> midiNotesPlayedList = null;

        // Dimensions variables
        int numberOfFrequences;
        int numberOfNotes;
        int totNumberOfNotes;

        /// <summary>
        /// Struct for storing results
        /// </summary>
        public struct temporalResultStruct
        {
            public double[] costFunctionArray;
            public Matrix temporalActivity;
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// Get the Temporal Activity Matrix
        /// </summary>
        public Matrix TemporalActivityMatrix
        {
            get
            {
                return temporalActivityMatrix;
            }
        }
        
        /// <summary>
        /// Get the array of Midi notes Played
        /// </summary>
        public int[] MidiNotesPlayed
        {
            get
            {
                return midiNotesPlayedList.ToArray();
            }
        }

        /// <summary>
        /// Get numer of Hops in frame
        /// </summary>
        public int NumberOfHopsFrame
        {
            get
            {
                return matrixY.NumberOfCol;
            }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor: Read dictionary
        /// </summary>
        /// <param name="normalizedCqt"></param>
        /// <param name="dictionaryPath"></param>
        public TemporalRecognition(double[,] normalizedCqt, string dictionaryPath)
        {
            // Get the dimension of frequences and times
            numberOfFrequences = normalizedCqt.GetLength(0);

            cqtTransform = normalizedCqt;

            // Read and store the dictionary
            using (StreamReader reader = new StreamReader(dictionaryPath))
            {
                // Read the dimension of matrix
                int numberA = int.Parse(reader.ReadLine());
                int numberF = int.Parse(reader.ReadLine());

                allNotesDictionaryMatrix = new Matrix(numberF, numberA);
                
                totNumberOfNotes = numberA;

                // read the value of frequences matrix
                for (int a = 0; a < numberA; a++)
                {
                    for (int f = 0; f < numberF; f++)
                    {
                        allNotesDictionaryMatrix.Value[f, a] = double.Parse(reader.ReadLine());
                    }
                }
            }
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Extract temporal activity
        /// </summary>
        /// <param name="hopSize">Size of hop used in CQT</param>
        /// <param name="sampleRate">Sample Rate of wav file</param>
        /// <param name="worker">Background worker</param>
        /// <returns>Temporal activity and cost function array</returns>
        public temporalResultStruct TranscriptionMethod(int hopSize, int sampleRate, BackgroundWorker worker)
        {
            // Extract dimension
            int numberOfHops = cqtTransform.GetLength(1);

            // Allocate memory for temporalActivity and cost function array
            Matrix temporalActivity = new Matrix(totNumberOfNotes, numberOfHops);
            double[] allHopsCostFuntion = new double[numberOfHops];

            // Parameter for the overlap between frame
            double overlapFactor =  1 - 0.25;
            // Frame duration in seconds
            double frameTime = 10.0;
            // Number of hops in frame
            int frameHops = (int)Math.Round((frameTime / hopSize) * sampleRate);
            // Compute the number of frames
            int overlappedFrame = (int)Math.Floor(frameHops * overlapFactor);
            int numberOfFrame = (int)(numberOfHops / overlappedFrame) + 1;

            // Begin transcription
            for (int frame = 0; frame < numberOfFrame; frame++)
            {
                if (worker != null && worker.CancellationPending)
                {
                    break;
                }
                else
                {
                    // Report progress bar
                    int perc = (int)Math.Floor((100.0 / numberOfFrame) * frame);
                    worker.ReportProgress(perc);

                    // Export the start of frame
                    int startOfFrame = frame * overlappedFrame;

                    // Number of hops in frame
                    int cqtHops = Math.Min(frameHops, numberOfHops - startOfFrame);
                    
                    // Extract CQT in current frame
                    double[,] frameCQT = new double[numberOfFrequences, cqtHops];
                    for (int f = 0; f < numberOfFrequences; f++)
                    {
                        for (int hop = 0; hop < cqtHops; hop++)
                        {
                            frameCQT[f, hop] = cqtTransform[f, (hop + startOfFrame)];
                        }
                    }

                    // Inizialize matrixY with the frameCqt selected
                    matrixY = new Matrix(numberOfFrequences, cqtHops);
                    matrixY.Value = frameCQT;

                    // Inizialize the midi Notes Played list with all
                    // notes from C1 to B6
                    midiNotesPlayedList = new List<int>();
                    midiNotesPlayedList.Clear();
                    for (int n = 24; n < 96; n++)
                    {
                        midiNotesPlayedList.Add(n);
                    }

                    // Inizialize the notes Dictionary
                    notesDictionaryMatrix = allNotesDictionaryMatrix;

                    // First step: Quadratic optimization
                    ComputeTemporalActivityQuadratic(worker);
                    //ComputeTemporalActivityDivergence(worker);
                    // Variable for reducing dictionary
                    Matrix reducedMatrixF = notesDictionaryMatrix;
                    Matrix oldReducedMatrixF = reducedMatrixF;

                    // Second step: Reduce dictionary and apply divergence optimization
                    while (true)
                    {
                        // Reduce dictionary
                        reducedMatrixF = this.ReduceDictionaryMatrix();

                        // Extract dimension of old and new dictionary and compare them
                        int noteSpectrumLength = oldReducedMatrixF.NumberOfCol;
                        int reducedSpectrumLength = reducedMatrixF.NumberOfCol;
                        if (noteSpectrumLength == reducedSpectrumLength)
                        {
                            break;
                        }

                        // Divergence optimization
                        this.ComputeTemporalActivityDivergence(worker);

                        oldReducedMatrixF = reducedMatrixF;
                    }

                    // Extract partial temporal activity values
                    Matrix partialTemporalActivity = this.TemporalActivityMatrix;
                    int[] partialNotesPlayed = this.MidiNotesPlayed;
                    double[] partialCostFuntion = this.costFunctionValue;

                    // Update temporalActivity Matrix and cost function array
                    for (int a = 0; a < partialNotesPlayed.Length; a++)
                    {
                        int notePlayed = partialNotesPlayed[a] - 24;
                        for (int hop = 0; hop < cqtHops; hop++)
                        {
                            temporalActivity.Value[notePlayed, hop + startOfFrame] = partialTemporalActivity.Value[a, hop];
                            allHopsCostFuntion[hop + startOfFrame] = partialCostFuntion[hop];
                        }
                    }
                }
            }

            // Store result
            temporalResultStruct resultStruct = new temporalResultStruct();
            resultStruct.costFunctionArray = allHopsCostFuntion;
            resultStruct.temporalActivity = temporalActivity;

            return resultStruct;
        }

        /// <summary>
        /// Optimization by projected gradient and quadratic cost function
        /// </summary>
        /// <param name="worker">Background worker for cancel task and progress report</param>
        public void ComputeTemporalActivityQuadratic(BackgroundWorker worker)
        {             
            // extract the number of notes effectively played
            numberOfNotes = notesDictionaryMatrix.NumberOfCol;
            int nHop = this.NumberOfHopsFrame;
            // Generate the Temporal Activity Matrix with random numbers in [0,1]
            temporalActivityMatrix = new Matrix(numberOfNotes, nHop);
            costFunctionValue = new double[nHop];

            
            // Compute Optimization Hop by hop
            Parallel.For(0, nHop, new ParallelOptions { MaxDegreeOfParallelism = 8 }, (hop, loopState) =>
            {
                if (worker != null && worker.CancellationPending)
                {
                    loopState.Break();
                }
                else
                {
                    // Extract the 'hop' column
                    Matrix vectorY = matrixY.GetColumn(hop);
                    OptimizationFunction.outMinimization outOptimization = OptimizationFunction.MinimizeQuadraticFunction(vectorY, notesDictionaryMatrix);

                    // Update the column of temporal matrix at the stop of optimization
                    for (int a = 0; a < numberOfNotes; a++)
                    {
                        temporalActivityMatrix.Value[a, hop] = outOptimization.vectorX.Value[a, 0];
                        costFunctionValue[hop] = outOptimization.costFunctionValue;
                    }
                }
            });
        }

        /// <summary>
        /// Optimization by projected gradient and Divergenze cost function 
        /// </summary>
        /// <param name="worker">Background worker for cancel task and progress report</param>
        public void ComputeTemporalActivityDivergence(BackgroundWorker worker)
        {
            // extract the number of notes effectively played
            numberOfNotes = notesDictionaryMatrix.NumberOfCol;
            int nHops = this.NumberOfHopsFrame;

            // Generate the Temporal Activity Matrix with random numbers in [0,1]
            temporalActivityMatrix = new Matrix(numberOfNotes, nHops);

            // Compute Optimization Hop by hop
            Parallel.For(0, nHops, new ParallelOptions { MaxDegreeOfParallelism = 8 }, (hop, loopState) =>
            {
                if (worker != null && worker.CancellationPending)
                {
                    loopState.Break();
                }
                else
                {
                    // Extract the 'hop' column
                    Matrix vectorY = matrixY.GetColumn(hop);
                    OptimizationFunction.outMinimization outOptimization = OptimizationFunction.MinimizeDivergenceFunction(vectorY, notesDictionaryMatrix);

                    // Update the column of temporal matrix at the stop of optimization
                    for (int a = 0; a < numberOfNotes; a++)
                    {
                        temporalActivityMatrix.Value[a, hop] = outOptimization.vectorX.Value[a, 0];
                        costFunctionValue[hop] = outOptimization.costFunctionValue;
                    }
                }
            });
        }     

        /// <summary>
        /// Reduce frequencies, ignoring all notes
        /// having the max of temporal activity less then 0.2
        /// </summary>
        /// <returns>Reduced dictionary matrix</returns>
        public Matrix ReduceDictionaryMatrix()
        {
            // List to store the notes played
            List<int> notesPlayedList = new List<int>();
            // List to store the notes not played
            List<int> notesToDelete = new List<int>();
            // out array
            int[] notesPlayed;

            int nHops = this.NumberOfHopsFrame;

            Matrix redusedNotesDictionary;
            Matrix redusedTemporalActivity;
            // Normalize temporal activity
            Matrix normalizedTemporalActivity = temporalActivityMatrix.LinearNormalization();

            for (int a = 0; a < numberOfNotes; a++)
            {
                // find the max value for all a
                double max = 0.0;
                for (int hop = 0; hop < nHops; hop++)
                {
                    if (normalizedTemporalActivity.Value[a, hop] > max)
                    {
                        max = normalizedTemporalActivity.Value[a, hop];
                    }
                }
                // if max > 0.2 the note is effectively played, else not.
                if (max > 0.2)
                {
                    notesPlayedList.Add(a);
                }
                else
                {
                    notesToDelete.Add(a);
                }
            }

            // remove the not played notes form midi notes list
            for (int a = notesToDelete.Count - 1; a >= 0; a--)
            {
                midiNotesPlayedList.RemoveAt(notesToDelete.ElementAt(a));
            }
            notesToDelete.Clear();
            notesPlayed = notesPlayedList.ToArray();
            notesPlayedList.Clear();

            redusedNotesDictionary = new Matrix(numberOfFrequences, notesPlayed.Length);
            redusedTemporalActivity = new Matrix(notesPlayed.Length, nHops);

            for (int a = 0; a < notesPlayed.Length; a++)
            {
                // select index
                int aIndex = notesPlayed[a];
                
                // store the selected notes
                for (int f = 0; f < numberOfFrequences; f++)
                {
                    redusedNotesDictionary.Value[f, a] = notesDictionaryMatrix.Value[f, aIndex];
                }

                // store the temporal activity of notes played
                for (int hop = 0; hop < nHops; hop++)
                {
                    redusedTemporalActivity.Value[a, hop] = temporalActivityMatrix.Value[aIndex, hop];
                }

            }
            // update temporal activity and return reduced dictioanry.
            
            temporalActivityMatrix = redusedTemporalActivity;
            notesDictionaryMatrix = redusedNotesDictionary;
            return redusedNotesDictionary;
        }

        # region HMM Post Processing

        /// <summary>
        /// HMM Postprocessing: Extract piano roll matrix (1 = note on; 0 = note off)
        /// </summary>
        /// <param name="temporalActivity">Temporal activity for extract emission probability</param>
        /// <param name="worker">Background worker</param>
        /// <returns>Piano Roll Matrix</returns>
        public Matrix GetMidiActivityMatrix(Matrix temporalActivity, BackgroundWorker worker)
        {
            // Extract dimensions
            int numberOfNotes = temporalActivity.NumberOfRows;
            int numberOfHops = temporalActivity.NumberOfCol;

            // Inizialize out matrix
            Matrix outMidiActivity = new Matrix(numberOfNotes, numberOfHops);

            // Start post processing
            for (int note = 0; note < numberOfNotes; note++)
            {
                // Report progress
                int perc = (int)Math.Floor((100.0 / numberOfNotes) * note);
                worker.ReportProgress(perc);

                // Read and stor transition matrix for HMM
                double[,] transitionMatrix = new double[2, 2];
                using (StreamReader sr = new StreamReader("Data\\HMM\\hmm" + (note+1).ToString() + ".dat"))
                {
                    for (int i = 0; i < 2; i++)
                    {
                        for (int j = 0; j < 2; j++)
                        {
                            transitionMatrix[i, j] = double.Parse(sr.ReadLine().Replace(".",","));
                        }
                    }
                }

                // Extract note-ma temporal activity
                double[] noteActivity = new double[numberOfHops];
                for (int hop = 0; hop < numberOfHops; hop++)
                {
                    noteActivity[hop] = temporalActivity.Value[note, hop];
                }

                // Compute out piano roll for note-ma note.
                double[] outSequence = OptimizationFunction.ViterbiHmm(noteActivity, transitionMatrix, 0);

                // Store activity
                for (int hop = 0; hop < numberOfHops; hop++)
                {
                    outMidiActivity.Value[note, hop] = outSequence[hop];
                }
            }
            return outMidiActivity;
        }

        /// <summary>
        /// Get onset matrix from piano roll matrix. onset = positive edge
        /// </summary>
        /// <param name="midiActivity">Piano roll matrix</param>
        /// <returns>Onset Matrix</returns>
        public Matrix GetOnsetMatrixHmm(Matrix midiActivity)
        {
            int numberOfNotes = midiActivity.NumberOfRows;
            int numberOfHops = midiActivity.NumberOfCol;

            Matrix onsetMatrix = new Matrix(numberOfNotes, numberOfHops);

            for (int note = 0; note < numberOfNotes; note++)
            {
                int state = 0;
                int oldState = state;
                for (int hop = 0; hop < numberOfHops; hop++)
                {
                    state = (int)midiActivity.Value[note, hop];
                    if (oldState == 0 && state == 1)
                    {
                        onsetMatrix.Value[note, hop] = 1;
                    }
                    oldState = state;
                }
            }
            return onsetMatrix;
        }

        /// <summary>
        /// Get offset matrix from piano roll matrix. offset = negative edge
        /// </summary>
        /// <param name="midiActivity">Piano roll matrix</param>
        /// <returns>Offset Matrix</returns>
        public Matrix GetOffsetMatrixHmm(Matrix midiActivity)
        {
            int numberOfNotes = midiActivity.NumberOfRows;
            int numberOfHops = midiActivity.NumberOfCol;

            Matrix offsetMatrix = new Matrix(numberOfNotes, numberOfHops);

            for (int note = 0; note < numberOfNotes; note++)
            {
                int state = 0;
                int oldState = state;
                for (int hop = 0; hop < numberOfHops; hop++)
                {
                    state = (int)midiActivity.Value[note, hop];
                    if (oldState == 1 && state == 0)
                    {
                        offsetMatrix.Value[note, hop] = -1;
                    }
                    oldState = state;
                }

            }
            return offsetMatrix;
        }

        #endregion

        #region Threshold Post-Processing

        /// <summary>
        /// Threshold Post-Processing: Get the onset matrix for generation of midi
        /// </summary>
        /// <param name="derivateMatrix">Derivate mof temporal activity</param>
        /// <param name="windowsLength">Window size for local maximum search</param>
        /// <returns>Onset Matrix</returns>
        public Matrix GetOnsetMatrix(Matrix derivateMatrix, Matrix temporalActivity, int windowsLength, BackgroundWorker worker)
        {
            // Extract dimensions
            int numberOfNotes = derivateMatrix.NumberOfRows;
            int numberOfHops = derivateMatrix.NumberOfCol;

            // Inizialize onset matrix
            Matrix onsetMatrix = new Matrix(numberOfNotes, numberOfHops);

            // List to find local maximum in derivate
            List<double> derivateRowList = new List<double>();

            for (int note = 0; note < numberOfNotes; note++)
            {
                // Report progress
                int perc = (int)Math.Floor((100.0 / numberOfNotes) * note);
                worker.ReportProgress(perc);

                // List for extract window from derivate list.
                List<double> windowRowList = new List<double>();

                // Extract the hop-sima row of derivate Matrix
                for (int hop = 0; hop < numberOfHops + windowsLength; hop++)
                {
                    if (hop < numberOfHops)
                    {
                        derivateRowList.Add(derivateMatrix.Value[note, hop]);
                    }
                    else
                    {
                        derivateRowList.Add(Constants.eps);
                    }
                }

                // for all hops extract a window of length range and find the local maxima,
                // if it is major than threshold start onset detection
                
                // Memorize old index of max value
                int oldMaxIndex = 0;
                for (int hop = 0; hop < numberOfHops; hop += windowsLength / 2)
                {
                    // index of onset candidate
                    int onsetIndex;

                    // Set range for window extraction
                    int range = Math.Min(windowsLength, numberOfHops - hop);

                    // Extract window
                    windowRowList = derivateRowList.GetRange(hop, range);

                    // Extract local maximum in derivate
                    double localDerivateMaxValue = windowRowList.Max();
                    // Extract index of local maximum in derivate
                    onsetIndex = windowRowList.IndexOf(localDerivateMaxValue) + hop;

                    // Set threshold for maximum derivate candidate
                    double medianValue = windowRowList.OrderBy(x => x).ElementAt(range / 2);
                    double threshold = 0.1 + medianValue;

                    if (localDerivateMaxValue > threshold)
                    {
                        // Search maximum value of note activity 
                        int maxIndex = 0;
                        // Start search from onset candidate index
                        int hopIndex = onsetIndex;
                        // max envelope value
                        double maxEnvelopeValue = double.NegativeInfinity;

                        // Stop after two consecutive true
                        int consecutiveTrue = 0;
                        while (consecutiveTrue < 2 && hopIndex < numberOfHops)
                        {
                            // Store maxiumum value if temporal activity is great of current maximum value
                            if (temporalActivity.Value[note, hopIndex] > maxEnvelopeValue)
                            {
                                maxEnvelopeValue = temporalActivity.Value[note, hopIndex];
                                maxIndex = hopIndex;
                                consecutiveTrue = 0;
                            }
                            // Increase consecutive true flag
                            else
                            {
                                consecutiveTrue++;
                            }
                            hopIndex++;
                        }

                        // store onset only if refer to a different maximum value of previous iteration,
                        // and if max envelope value is greather than thrshold
                        if (oldMaxIndex != maxIndex)
                        {
                            if (temporalActivity.Value[note, maxIndex] > 0.2)
                            {
                                onsetMatrix.Value[note, onsetIndex] = maxEnvelopeValue;
                            }
                        }
                        // memorize index of max value
                        oldMaxIndex = maxIndex;
                    }
                    // clear list
                    windowRowList.Clear();
                }
                // clear list
                derivateRowList.Clear();
            }
            return onsetMatrix;
        }

        /// <summary>
        /// Threshold Post-Processing: Get the offset matrix for generation of midi
        /// </summary>
        /// <param name="onsetMatrix">Onset matrix precius computed</param>
        /// <param name="temporalActivity">Temporal activity</param>
        /// <param name="hopTime">Duration of hops in seconds</param>
        /// <returns>Offset Matrix</returns>
        public Matrix GetOffsetMatrixTh(Matrix onsetMatrix, Matrix temporalActivity, double hopTime)
        {
            // Default threshold factor
            double thresholdFactor = 0.5;

            // Extract dimensions
            int numberOfNotes = onsetMatrix.NumberOfRows;
            int numberOfHops = onsetMatrix.NumberOfCol;

            // Initialize offset matrix
            Matrix offsetMatrix = new Matrix(numberOfNotes, numberOfHops);

            // find for all notes the onset timee
            for (int note = 0; note < numberOfNotes; note++)
            {
                for (int hop = 0; hop < numberOfHops; hop++)
                {
                    if (onsetMatrix.Value[note, hop] != 0)
                    {
                        // extract temporal activity between two consecutive onsets
                        List<double> consecutiveOnsetRange = new List<double>();
                        for (int w = hop + 1; w < numberOfHops; w++)
                        {
                            if (onsetMatrix.Value[note, w] == 0)
                            {
                                consecutiveOnsetRange.Add(temporalActivity.Value[note, w]);
                            }
                            else
                            {
                                break;
                            }
                        }

                        // Reduce threshold factor for each 2 seconds between two onset
                        int thDist = (int)Math.Round(2.0 / hopTime);
                        switch (consecutiveOnsetRange.Count / thDist)
                        {
                            case 0:
                                thresholdFactor = 0.5;
                                break;

                            case 1:
                                thresholdFactor = 0.4;
                                break;

                            case 2:
                                thresholdFactor = 0.3;
                                break;

                            case 3:
                                thresholdFactor = 0.2;
                                break;

                            case 4:
                                thresholdFactor = 0.1;
                                break;

                            default:
                                thresholdFactor = 0.1;
                                break;
                        }

                        // Extract max value of current note
                        int maxIndex = 0;
                        int hopIndex = hop;
                        double maxEnvelopeValue = double.NegativeInfinity;

                        int consecutiveTrue = 0;
                        while (consecutiveTrue < 2 && hopIndex < numberOfHops)
                        {
                            if (temporalActivity.Value[note, hopIndex] > maxEnvelopeValue)
                            {
                                maxEnvelopeValue = temporalActivity.Value[note, hopIndex];
                                maxIndex = hopIndex;
                                consecutiveTrue = 0;
                            }
                            else
                            {
                                consecutiveTrue++;
                            }
                            hopIndex++;
                        }

                        // Set threshold value
                        double threshold = thresholdFactor * temporalActivity.Value[note, maxIndex];

                        for (int w = hop + 1; w < numberOfHops; w++)
                        {
                            // if temporal activity go under threshold, store offset.
                            if (temporalActivity.Value[note, w] < threshold)
                            {
                                offsetMatrix.Value[note, w] = -1;
                                break;
                            }

                            // If no offset is detected before following onset, set the offset one hop before
                            // subseguent onset.
                            if (onsetMatrix.Value[note, w] != 0)
                            {
                                offsetMatrix.Value[note, w - 1] = -1;
                                break;
                            }
                        }
                    }
                }
            }
            return offsetMatrix;
        }

        #endregion

        #endregion
    }
}
