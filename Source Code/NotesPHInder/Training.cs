﻿// Copyright (c) 2015, 2016 Massimiliano Luzi, Mario Antonelli and Antonello Rizzi
// University of Rome "La Sapienza"
//
// massimiliano.luzi@uniroma1.it
// mario.antonelli@gmail.com 
// antonello.rizzi@uniroma1.it 
//
// This file is part of NotesPHInder.
//
// NotesPHInder is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// NotesPHInder is distributed in the hope that it will be useful. 
// IT IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
// PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
// CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with NotesPHInder.If not, see<http://www.gnu.org/licenses/>.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Media;
using System.IO;

namespace NotesPHInder
{
    /// <summary>
    /// Class for Training Task
    /// </summary>
    class Training
    {
        #region Private Methods

        /// <summary>
        /// The function return the cqt index related to the fundamental frequency of the current note
        /// </summary>
        /// <param name="spectrum">The spectral representation of the current note</param>
        /// <param name="threshold">The threshold for detecting the first harmonic</param>
        /// <returns></returns>
        private static int GetNoteIndex(double[] spectrum, double threshold)
        {
            // get dimension
            int spectrumLength = spectrum.Length;

            // Initialize variables

            int outIndex;                                           // output variable
            double[] derivative = new double[spectrumLength];       // array for storing the derivative of the spectrum
            List<double> peaksFrequenceList = new List<double>();   // List storing the detecteck frequncy peak
            List<double> deltaFrequenceList = new List<double>();   // List fr storing the distance between consecutive peaks

            // Evaluate the spectrum derivative
            for (int f = 0; f < spectrumLength - 1; f++)
            {
                derivative[f] = spectrum[f + 1] - spectrum[f];
            }

            // Search for the frequency peaks
            for (int f = 1; f < spectrumLength; f++)
            {
                // If a zero crossing happens and the sepctrum value is greater than the threshold then a peak is detected.
                if (derivative[f - 1] >= 0 && derivative[f] <= 0 && spectrum[f] >= threshold)
                {
                    // Get the frequency of the detected peak
                    double freq = Constants.minFrequence * Math.Pow(2, (double)(f) / Constants.binsPerOctave);
                    peaksFrequenceList.Add(freq);
                    if (peaksFrequenceList.Count == 2)
                    {
                        break;
                    }
                }
            }

            // Perform a renforced detecting. If 
            double fundamentalFreq = peaksFrequenceList[0];
            double renforceFundamentalFreq;
            if (peaksFrequenceList.Count == 2)
            {
                renforceFundamentalFreq = peaksFrequenceList[1] - peaksFrequenceList[0];
            }
            else
            {
                renforceFundamentalFreq = peaksFrequenceList[0];
            }

            if (Math.Abs(fundamentalFreq - renforceFundamentalFreq) < Math.Pow(10, -3))
            {
                fundamentalFreq = (fundamentalFreq + renforceFundamentalFreq) / 2;
            }
            outIndex = (int)Math.Round(Constants.binsPerOctave * Math.Log(fundamentalFreq / Constants.minFrequence, 2));

            return outIndex;
        }

        #endregion

        #region Global Varibles

        // Set default number of vocables
        static readonly int numberOfVocables = 72;

        // Declare public variables
        int numberOfHarmonics;
        int maxWindowSize;
        int maxFrequence;
        int numFrequences;
        int hopSize;
        int sampleRate;
        CQT cqtNotesSynthesis = null;
        CQT cqtHarmonicsExtraction = null;
        string trainingPath;

        #endregion

        #region Private Methods

        /// <summary>
        /// Median filter to array
        /// </summary>
        /// <param name="array"></param>
        /// <param name="filterLength"></param>
        /// <returns></returns>
        private double[] MedianFilter(double[] array, int filterLength)
        {
            Queue<double> fifo = new Queue<double>(filterLength);
            
            int leftBound = (int)Math.Ceiling((double)(filterLength / 2));
            int rightBound = filterLength / 2;
            int outIndex = filterLength / 2;

            double[] outArray = new double[array.Length];

            // Inizialize fifo
            for (int w = 0; w < filterLength; w++)
            {
                if (w < leftBound)
                {
                    fifo.Enqueue(array[0]);
                }
                else
                {
                    fifo.Enqueue(array[w - leftBound + 1]);
                }
            }

            for (int w = 0; w < array.Length; w++)
            {
                List<double> orderedFifo;

                // Add element in fifo and delete the last one
                if (w + rightBound + 1 < array.Length)
                {
                    fifo.Enqueue(array[w + rightBound + 1]);
                }
                else
                {
                    fifo.Enqueue(array[array.Length - 1]);
                }
                fifo.Dequeue();

                // order elements
                orderedFifo = new List<double>(fifo.OrderBy(x => x));

                // Extract the middle element
                outArray[w] = orderedFifo.ElementAt(outIndex);
            }
            // Clear the fifo before next row
            fifo.Clear();
            
            return outArray;
        }    

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor: Set path of wave files for training
        /// </summary>
        /// <param name="path">Path where are stored wav files for training</param>
        public Training(string path)
        {
            trainingPath = path;
        }

        #endregion

        # region Public Methods

        /// <summary>
        /// Training method
        /// </summary>
        /// <param name="worker">Background Worker for progress bar report</param>
        /// <returns>Notes Dictionary Matrix</returns>
        public double[][] ComputeTraining(BackgroundWorker worker)
        {
            string[] globalProgState = new[] { "global", "" };
            string[] localProgState = new[] { "local", "" };

            // Read samples
            string[] notesPath = Directory.GetFiles(trainingPath, "*.wav");
            int numberOfSamples = notesPath.Length;

            // Variables for training
            double[][] dictionaryMatrix = new double[numberOfVocables][];
            double[][] noteSpectrum = new double[numberOfSamples][];
            double[][] harmonics = new double[numberOfSamples][];

            // Variables for Constant Q Transform
            int binsPerOctave = Constants.binsPerOctave;
            double toneSpacing = Math.Pow(2, 1.0 / binsPerOctave);
            int Q = (int)Math.Ceiling(1.0 / (toneSpacing - 1));
            double hopTime = 0.01;

            // Extract harmonics from the notes samples
            for (int note = 0; note < numberOfSamples; note++)
            {
                if (worker != null && worker.CancellationPending)
                {
                    break;
                }
                else
                {
                    // Report local task
                    localProgState[1] = "Extracting harmonics from:" + Path.GetFileName(notesPath[note]);
                    worker.ReportProgress(-1, localProgState);

                    // Read the wave file and extract the envelope
                    WaveHandler wave = new WaveHandler(notesPath[note]);
                    sampleRate = wave.SampleRate;
                    short[] waveSamples = wave.ExtractMonoSample();
                    double[] normalizedSamples = wave.GetLinearNormalizedSamples(waveSamples);
                    double[] envelope = wave.GetEnvelope(normalizedSamples, 512);

                    // Reduce the samples length to time in witch envelope is less than 10% of maximum
                    List<double> reducedSamples = new List<double>();

                    int consecutiveTrue = 0;

                    // Search index of max value
                    int maxIndex = 0;
                    while (envelope[maxIndex] != 1)
                    {
                        reducedSamples.Add(normalizedSamples[maxIndex]);
                        maxIndex++;
                    }

                    for (int i = maxIndex; i < normalizedSamples.Length; i++)
                    {
                        reducedSamples.Add(normalizedSamples[i]);
                        if (envelope[i] < 0.01)
                        {
                            consecutiveTrue++;
                            if (consecutiveTrue > 1024)
                            {
                                break;
                            }
                        }
                        else
                        {
                            consecutiveTrue = 0;
                        }
                    }

                    // Store reduced samples and clear list
                    normalizedSamples = reducedSamples.ToArray();
                    reducedSamples.Clear();

                    // Constant Q Transform
                    // Set variables for CQT
                    maxWindowSize = (sampleRate / 10);

                    maxFrequence = (int)Math.Floor(sampleRate / 2.0);
                    numFrequences = (int)Math.Floor(Math.Log(maxFrequence / Constants.minFrequence) / Math.Log(toneSpacing));

                    hopSize = (int)Math.Round(sampleRate * hopTime);

                    // Compute CQT
                    cqtHarmonicsExtraction = new CQT(sampleRate, numFrequences, maxWindowSize, Q, toneSpacing);
                    double[,] cqtTransform = cqtHarmonicsExtraction.GetTranform(normalizedSamples, hopSize, worker);
                    double[,] normalizedCqt = Matrix.AffineNormalizeMatrix(cqtTransform);

                    // Extract Harmonics Task
                    int numberOfHops = cqtTransform.GetLength(1);

                    // Array of note spectrum
                    noteSpectrum[note] = new double[numFrequences];

                    // Extract spectral feature of note
                    double max = double.NegativeInfinity;
                    for (int f = 0; f < numFrequences; f++)
                    {

                        double sum = 0.0;
                        for (int hop = 0; hop < numberOfHops; hop++)
                        {
                            sum += normalizedCqt[f, hop];
                        }
                        noteSpectrum[note][f] = sum;

                        if (noteSpectrum[note][f] > max)
                        {
                            max = noteSpectrum[note][f];
                        }
                    }

                    // Normalize spectrum
                    for (int f = 0; f < numFrequences; f++)
                    {
                        noteSpectrum[note][f] = (noteSpectrum[note][f]) / (max);
                        if (noteSpectrum[note][f] == 0)
                        {
                            noteSpectrum[note][f] = Constants.eps;
                        }
                    }

                    // Get note Index
                    int noteIndex = GetNoteIndex(noteSpectrum[note], 0.2);

                    // Inizialize harmonics array
                    numberOfHarmonics = (int)Math.Ceiling((double)maxFrequence / Constants.minFrequence);
                    harmonics[note] = new double[numberOfHarmonics];
                    if (waveSamples.Max() == 0)
                    {
                        continue;
                    }
                    for (int a = 0; a < numberOfHarmonics; a++)
                    {
                        int offset = (int)Math.Round(binsPerOctave * Math.Log(a + 1, 2));
                        if (noteIndex + offset < numFrequences)
                        {
                            // Lower Bound = 1 / n
                            harmonics[note][a] = Math.Max(noteSpectrum[note][noteIndex + offset], 1.0 / (a + 1));
                        }
                        else
                        {
                            harmonics[note][a] = 1.0 / (a + 1);
                        }

                    }

                    // Report global progress
                    int perc = (int)Math.Floor((note + 1) * (100.0 / (12 + numberOfVocables)));
                    worker.ReportProgress(perc, globalProgState);
                }
            }

            // Synthesis Task
            cqtNotesSynthesis = new CQT(sampleRate, numFrequences, maxWindowSize, Q, toneSpacing);
            hopSize = (int)Math.Round(sampleRate * hopTime);
            for (int v = 0; v < numberOfVocables; v++)
            {
                if (worker != null && worker.CancellationPending)
                {
                    break;
                }
                else
                {
                    // Report local task
                    int midiNote = v + 24;
                    int octave = (midiNote / 12) - 1;
                    string noteName;
                    switch (midiNote % 12)
                    {
                        case 0:
                            noteName = "C" + octave.ToString();
                            break;

                        case 1:
                            noteName = "C#" + octave.ToString();
                            break;

                        case 2:
                            noteName = "D" + octave.ToString();
                            break;

                        case 3:
                            noteName = "D#" + octave.ToString();
                            break;

                        case 4:
                            noteName = "E" + octave.ToString();
                            break;

                        case 5:
                            noteName = "F" + octave.ToString();
                            break;

                        case 6:
                            noteName = "F#" + octave.ToString();
                            break;

                        case 7:
                            noteName = "G" + octave.ToString();
                            break;

                        case 8:
                            noteName = "G#" + octave.ToString();
                            break;

                        case 9:
                            noteName = "A" + octave.ToString();
                            break;

                        case 10:
                            noteName = "A#" + octave.ToString();
                            break;

                        case 11:
                            noteName = "B" + octave.ToString();
                            break;

                        default:
                            noteName = "";
                            break;
                    }
                    localProgState[1] = "Synthesizing: " + noteName;
                    worker.ReportProgress(-1, localProgState);

                    // Inzialize array for store dictionary matrix
                    dictionaryMatrix[v] = new double[numFrequences];

                    // Create 5 seconds signal
                    double[] signal = new double[5 * sampleRate];

                    // Array for store harmonics structure
                    double[] harmonicStructure = new double[numberOfHarmonics];

                    // Extract frequency of current harmonic 
                    double noteFreq = Constants.minFrequence * Math.Pow(2, (double)v / 12);
                    for (int a = 0; a < numberOfHarmonics; a++)
                    {
                        for (int note = 0; note < numberOfSamples; note++)
                        {
                            harmonicStructure[a] += harmonics[note][a];
                        }
                    }
                    // Normalize
                    double maxHarmonicValue = harmonicStructure.Max();
                    for (int a = 0; a < numberOfHarmonics; a++)
                    {
                        harmonicStructure[a] = harmonicStructure[a] / maxHarmonicValue;
                    }

                    // Syntetize signal
                    for (int t = 0; t < 5 * sampleRate; t++)
                    {
                        // Compute time
                        double time = (double)t / sampleRate;

                        // Signal generation
                        for (int a = 0; a < numberOfHarmonics; a++)
                        {
                            if (noteFreq * (a + 1) < maxFrequence)
                            {
                                signal[t] += harmonicStructure[a] * Math.Cos(2 * Math.PI * noteFreq * (a + 1) * time);
                            }
                        }

                    }

                    // Extract spectrum for synthetized signal
                    double[,] signalTrasnform = cqtNotesSynthesis.GetTranform(signal, hopSize, worker);
                    signalTrasnform = Matrix.AffineNormalizeMatrix(signalTrasnform);

                    int nHop = signalTrasnform.GetLength(1);
                    // Mean hops and normalize
                    double max = double.NegativeInfinity;
                    for (int f = 0; f < numFrequences; f++)
                    {
                        double sum = 0.0;
                        for (int hop = 0; hop < nHop; hop++)
                        {
                            sum += signalTrasnform[f, hop];
                        }
                        dictionaryMatrix[v][f] = sum;

                        if (dictionaryMatrix[v][f] > max)
                        {
                            max = dictionaryMatrix[v][f];
                        }
                    }
                    // Normalize
                    for (int f = 0; f < numFrequences; f++)
                    {
                        dictionaryMatrix[v][f] = (dictionaryMatrix[v][f]) / (max);
                        if (dictionaryMatrix[v][f] == 0)
                        {
                            dictionaryMatrix[v][f] = Constants.eps;
                        }
                    }
                    // Report global Progress
                    int perc = (int)Math.Floor(((v + 1) + 12) * (100.0 / (12 + numberOfVocables)));
                    worker.ReportProgress(perc, globalProgState);
                }
            }

            return dictionaryMatrix;
        }

        #endregion
    }
}
