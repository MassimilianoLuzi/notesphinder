﻿// Copyright (c) 2015, 2016 Massimiliano Luzi, Mario Antonelli and Antonello Rizzi
// University of Rome "La Sapienza"
//
// massimiliano.luzi@uniroma1.it
// mario.antonelli@gmail.com 
// antonello.rizzi@uniroma1.it 
//
// This file is part of NotesPHInder.
//
// NotesPHInder is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// NotesPHInder is distributed in the hope that it will be useful. 
// IT IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
// PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
// CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with NotesPHInder.If not, see<http://www.gnu.org/licenses/>.

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ILNumerics;
using ILNumerics.Drawing;
using ILNumerics.Drawing.Plotting;

namespace NotesPHInder
{
    /// <summary>
    /// Class for visualize graphic of trained dictionary
    /// </summary>
    public partial class TrainingImage : Form
    {
        float[,] picture;
        public TrainingImage(double[][] matrix)
        {
                        
            InitializeComponent();
            int numberV = matrix.GetLength(0);
            int numberF = matrix[1].Length;
            picture = new float[numberF, numberV];
            for (int v = 0; v < numberV; v++)
            {
                
                for (int f = 0; f < numberF; f++)
                {
                    picture[f,v] = (float)matrix[v][f];
                }
            }
            
            
        }


        private void ilPanel1_Load(object sender, EventArgs e)
        {
            ILScene scene = new ILScene();
            var surface = new ILSurface(picture, colormap: Colormaps.Jet);
            var plot = scene.Add(new ILPlotCube(twoDMode: false));
            plot.Add(surface);
            
            ilPanel1.Scene = scene;
        }
        
    }
}
