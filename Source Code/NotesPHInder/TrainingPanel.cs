﻿// Copyright (c) 2015, 2016 Massimiliano Luzi, Mario Antonelli and Antonello Rizzi
// University of Rome "La Sapienza"
//
// massimiliano.luzi@uniroma1.it
// mario.antonelli@gmail.com 
// antonello.rizzi@uniroma1.it 
//
// This file is part of NotesPHInder.
//
// NotesPHInder is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// NotesPHInder is distributed in the hope that it will be useful. 
// IT IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
// PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
// CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with NotesPHInder.If not, see<http://www.gnu.org/licenses/>.

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Media;

namespace NotesPHInder
{
    /// <summary>
    /// Class for Training Panel
    /// </summary>
    public partial class TrainingPanel : Form
    {
        #region Global Variebles

        bool cancelled;
        string trainingPath = null;
        double[][] vocableMatrix = null;

        #endregion

        #region Private Methods

        /// <summary>
        /// Start Training Button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void startAsyncButton_Click(System.Object sender,
            System.EventArgs e)
        {
            if (trainingPath != null)
            {
                this.startAsyncButton.Enabled = false;
                this.cancelAsyncButton.Enabled = true;
                this.sourceButton.Enabled = false;
                this.showDictionaryButton.Enabled = false;

                cancelled = false;
                trainingWorker.RunWorkerAsync();
            }
            else
            {
                MessageBox.Show("Select a valid directory");
            }
        }

        /// <summary>
        /// Cancel Training Button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cancelAsyncButton_Click(System.Object sender,
            System.EventArgs e)
        {
            // Cancel the asynchronous operation.
            this.trainingWorker.CancelAsync();
            // Disable the Cancel button.
            cancelAsyncButton.Enabled = false;
            startAsyncButton.Enabled = true;
            sourceButton.Enabled = true;
            showDictionaryButton.Enabled = false;
            cancelled = true;
        }

        /// <summary>
        /// Show Dictionary button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void showDictionaryButton_Click(object sender, EventArgs e)
        {
            if (vocableMatrix != null)
            {
                TrainingImage result = new TrainingImage(vocableMatrix);
                result.Show();
            }
        }

        /// <summary>
        /// Select dir of wave files for training
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SourceButton_Click(object sender, EventArgs e)
        {
            if (folderBrowserDialog.ShowDialog() == DialogResult.OK)
            {
                trainingPath = folderBrowserDialog.SelectedPath;
                dirLabel.Text = Path.GetFileName(trainingPath);
            }
        }

        /// <summary>
        /// Click Action for local progress bar
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void localProgressBar_Click(object sender, EventArgs e)
        {
            // Start folder dialog by clicking on progress bar
            if (localProgressBar.CustomText == "Select source directory" || localProgressBar.CustomText == "Cancelled")
            {
                if (folderBrowserDialog.ShowDialog() == DialogResult.OK)
                {
                    trainingPath = folderBrowserDialog.SelectedPath;
                    dirLabel.Text = Path.GetFileName(trainingPath);
                }
            }

            // Store dictionary by clicking on progress bar
            if (localProgressBar.CustomText == "Done. Click here to save dictionary")
            {
                string outPath;

                saveDictionaryDialog.Title = "Store the notes dictionary";
                saveDictionaryDialog.Filter = "dat files (*.dat)|*.dat|All files (*.*)|*.*";
                saveDictionaryDialog.InitialDirectory = Path.GetFullPath("Data\\Dictionaries\\");
                saveDictionaryDialog.AddExtension = false;
                saveDictionaryDialog.FilterIndex = 1;
                saveDictionaryDialog.AddExtension = true;

                MessageBox.Show("Make sure to save dictionary in default directory. If not, the program can't uses it for transcription.", "Warning");
                if (saveDictionaryDialog.ShowDialog() == DialogResult.OK)
                {
                    outPath = saveDictionaryDialog.FileName;

                    using (StreamWriter writer = new StreamWriter(outPath))
                    {
                        int numberV = vocableMatrix.Length;
                        int numberF = vocableMatrix[1].Length;

                        writer.WriteLine(numberV.ToString());
                        writer.WriteLine(numberF.ToString());

                        for (int v = 0; v < numberV; v++)
                        {
                            for (int f = 0; f < numberF; f++)
                            {
                                writer.WriteLine(vocableMatrix[v][f]);
                            }
                        }

                    }

                    // Select the new dictionary for transcription
                    string outDir = Path.GetDirectoryName(outPath);
                    string defaultDir = Path.GetFullPath("Data\\Dictionaries");
                    if (outDir == defaultDir)
                    {
                        string dictionaryName = Path.GetFileNameWithoutExtension(outPath);
                        StartPanel panel = (StartPanel)Owner;
                        panel.selectDictionary.Items.Add(dictionaryName);
                        panel.selectDictionary.SelectedIndex = panel.selectDictionary.Items.IndexOf(dictionaryName);
                    }
                }
            }
        }

        #region Training Worker

        /// <summary>
        /// Training worker: Do Work
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TrainingWorker_DoWork(object sender,
            DoWorkEventArgs e)
        {
            Training dictionaryTrainer = new Training(trainingPath);

            double[][] resultMatrix = dictionaryTrainer.ComputeTraining(trainingWorker);
            e.Result = resultMatrix;
        }

        /// <summary>
        /// Training Worker: Training complete
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TrainingWorker_RunWorkerCompleted(
            object sender, RunWorkerCompletedEventArgs e)
        {
            // First, handle the case where an exception was thrown.
            if (cancelled)
            {
                localProgressBar.Value = 0;
                localProgressBar.CustomText = "Cancelled";
                localProgressBar.Refresh();

                globalProgressBar.Value = 0;
            }
            else
            {
                // Finally, handle the case where the operation 
                // succeeded.
                localProgressBar.Value = 0;
                localProgressBar.CustomText = "Done. Click here to save dictionary";
                localProgressBar.Refresh();

                globalProgressBar.Value = 0;

                vocableMatrix = (double[][])e.Result;

                string outPath;

                saveDictionaryDialog.Title = "Store the notes dictionary";
                saveDictionaryDialog.Filter = "dat files (*.dat)|*.dat|All files (*.*)|*.*";
                saveDictionaryDialog.InitialDirectory = Path.GetFullPath("Data\\Dictionaries\\");
                saveDictionaryDialog.AddExtension = false;
                saveDictionaryDialog.FilterIndex = 1;
                saveDictionaryDialog.AddExtension = true;

                MessageBox.Show("Make sure to save dictionary in default directory. If not, the program can't uses it for transcription.", "Warning");
                if (saveDictionaryDialog.ShowDialog() == DialogResult.OK)
                {
                    outPath = saveDictionaryDialog.FileName;

                    using (StreamWriter writer = new StreamWriter(outPath))
                    {
                        int numberV = vocableMatrix.Length;
                        int numberF = vocableMatrix[1].Length;

                        writer.WriteLine(numberV.ToString());
                        writer.WriteLine(numberF.ToString());

                        for (int v = 0; v < numberV; v++)
                        {
                            for (int f = 0; f < numberF; f++)
                            {
                                writer.WriteLine(vocableMatrix[v][f]);
                            }
                        }

                    }

                    // Select the new dictionary for transcription
                    string outDir = Path.GetDirectoryName(outPath);
                    string defaultDir = Path.GetFullPath("Data\\Dictionaries");
                    if (outDir == defaultDir)
                    {
                        string dictionaryName = Path.GetFileNameWithoutExtension(outPath);
                        StartPanel panel = (StartPanel)Owner;
                        panel.selectDictionary.Items.Add(dictionaryName);
                        panel.selectDictionary.SelectedIndex = panel.selectDictionary.Items.IndexOf(dictionaryName);
                    }
                }
            }

            startAsyncButton.Enabled = true;
            cancelAsyncButton.Enabled = false;
            sourceButton.Enabled = true;
            showDictionaryButton.Enabled = true;
        }

        /// <summary>
        /// This event handler updates the progress bar.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TrainingWorker_ProgressChanged(object sender,
            ProgressChangedEventArgs e)
        {
            string[] state = (string[])e.UserState;
            if (state[0] == "local")
            {
                if (e.ProgressPercentage == -1)
                {
                    localProgressBar.CustomText = state[1];
                }
                else
                {
                    localProgressBar.Value = e.ProgressPercentage;
                }

            }
            if (state[0] == "global")
            {
                globalProgressBar.Value = e.ProgressPercentage;
            }
        }

        #endregion

        #endregion

        #region Constructor

        /// <summary>
        /// Training Panel Constructor
        /// </summary>
        public TrainingPanel()
        {
            InitializeComponent();

            trainingWorker.WorkerReportsProgress = true;
            trainingWorker.WorkerSupportsCancellation = true;

            cancelled = false;

            startAsyncButton.Enabled = true;
            cancelAsyncButton.Enabled = false;
            sourceButton.Enabled = true;
            showDictionaryButton.Enabled = false;
        }

        #endregion       
    }
}
