﻿// Copyright (c) 2015, 2016 Massimiliano Luzi, Mario Antonelli and Antonello Rizzi
// University of Rome "La Sapienza"
//
// massimiliano.luzi@uniroma1.it
// mario.antonelli@gmail.com 
// antonello.rizzi@uniroma1.it 
//
// This file is part of NotesPHInder.
//
// NotesPHInder is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// NotesPHInder is distributed in the hope that it will be useful. 
// IT IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
// PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
// CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with NotesPHInder.If not, see<http://www.gnu.org/licenses/>.

namespace NotesPHInder
{
    partial class TrainingPanel
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TrainingPanel));
            this.startAsyncButton = new System.Windows.Forms.Button();
            this.trainingWorker = new System.ComponentModel.BackgroundWorker();
            this.cancelAsyncButton = new System.Windows.Forms.Button();
            this.folderBrowserDialog = new System.Windows.Forms.FolderBrowserDialog();
            this.sourceButton = new System.Windows.Forms.Button();
            this.saveDictionaryDialog = new System.Windows.Forms.SaveFileDialog();
            this.dirLabel = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.showDictionaryButton = new System.Windows.Forms.Button();
            this.globalProgressBar = new NotesPHInder.CustomProgressBar();
            this.localProgressBar = new NotesPHInder.CustomProgressBar();
            this.SuspendLayout();
            // 
            // startAsyncButton
            // 
            this.startAsyncButton.Location = new System.Drawing.Point(12, 12);
            this.startAsyncButton.Name = "startAsyncButton";
            this.startAsyncButton.Size = new System.Drawing.Size(140, 23);
            this.startAsyncButton.TabIndex = 0;
            this.startAsyncButton.Text = "Start Learning";
            this.startAsyncButton.UseVisualStyleBackColor = true;
            this.startAsyncButton.Click += new System.EventHandler(this.startAsyncButton_Click);
            // 
            // trainingWorker
            // 
            this.trainingWorker.DoWork += new System.ComponentModel.DoWorkEventHandler(this.TrainingWorker_DoWork);
            this.trainingWorker.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.TrainingWorker_ProgressChanged);
            this.trainingWorker.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.TrainingWorker_RunWorkerCompleted);
            // 
            // cancelAsyncButton
            // 
            this.cancelAsyncButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cancelAsyncButton.Location = new System.Drawing.Point(179, 12);
            this.cancelAsyncButton.Name = "cancelAsyncButton";
            this.cancelAsyncButton.Size = new System.Drawing.Size(140, 23);
            this.cancelAsyncButton.TabIndex = 5;
            this.cancelAsyncButton.Text = "Cancel";
            this.cancelAsyncButton.UseVisualStyleBackColor = true;
            this.cancelAsyncButton.Click += new System.EventHandler(this.cancelAsyncButton_Click);
            // 
            // sourceButton
            // 
            this.sourceButton.Location = new System.Drawing.Point(12, 99);
            this.sourceButton.Name = "sourceButton";
            this.sourceButton.Size = new System.Drawing.Size(140, 23);
            this.sourceButton.TabIndex = 6;
            this.sourceButton.Text = "Source";
            this.sourceButton.UseVisualStyleBackColor = true;
            this.sourceButton.Click += new System.EventHandler(this.SourceButton_Click);
            // 
            // dirLabel
            // 
            this.dirLabel.AutoSize = true;
            this.dirLabel.Location = new System.Drawing.Point(13, 145);
            this.dirLabel.Name = "dirLabel";
            this.dirLabel.Size = new System.Drawing.Size(0, 13);
            this.dirLabel.TabIndex = 12;
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(9, 125);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(310, 56);
            this.label1.TabIndex = 15;
            this.label1.Text = "- Record some monophonic notes and save it a directory.\r\n- Use a 8 KHz Sample Rat" +
    "e and 16 bps.\r\n- Select the directory where the samples are stored and start the" +
    " learning process  ";
            // 
            // showDictionaryButton
            // 
            this.showDictionaryButton.Location = new System.Drawing.Point(179, 99);
            this.showDictionaryButton.Name = "showDictionaryButton";
            this.showDictionaryButton.Size = new System.Drawing.Size(140, 23);
            this.showDictionaryButton.TabIndex = 16;
            this.showDictionaryButton.Text = "Show Dictionary";
            this.showDictionaryButton.UseVisualStyleBackColor = true;
            this.showDictionaryButton.Click += new System.EventHandler(this.showDictionaryButton_Click);
            // 
            // globalProgressBar
            // 
            this.globalProgressBar.CustomText = null;
            this.globalProgressBar.DisplayStyle = NotesPHInder.ProgressBarDisplayText.Percentage;
            this.globalProgressBar.Location = new System.Drawing.Point(12, 70);
            this.globalProgressBar.Name = "globalProgressBar";
            this.globalProgressBar.Size = new System.Drawing.Size(307, 23);
            this.globalProgressBar.TabIndex = 14;
            // 
            // localProgressBar
            // 
            this.localProgressBar.CustomText = "Select source directory";
            this.localProgressBar.DisplayStyle = NotesPHInder.ProgressBarDisplayText.CustomText;
            this.localProgressBar.Location = new System.Drawing.Point(12, 41);
            this.localProgressBar.Name = "localProgressBar";
            this.localProgressBar.Size = new System.Drawing.Size(307, 23);
            this.localProgressBar.TabIndex = 13;
            this.localProgressBar.Click += new System.EventHandler(this.localProgressBar_Click);
            // 
            // TrainingPanel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(331, 190);
            this.Controls.Add(this.showDictionaryButton);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.globalProgressBar);
            this.Controls.Add(this.localProgressBar);
            this.Controls.Add(this.dirLabel);
            this.Controls.Add(this.sourceButton);
            this.Controls.Add(this.cancelAsyncButton);
            this.Controls.Add(this.startAsyncButton);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "TrainingPanel";
            this.Text = "Dictionary Learning";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button startAsyncButton;
        private System.ComponentModel.BackgroundWorker trainingWorker;
        private System.Windows.Forms.Button cancelAsyncButton;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog;
        private System.Windows.Forms.Button sourceButton;
        private System.Windows.Forms.SaveFileDialog saveDictionaryDialog;
        private System.Windows.Forms.Label dirLabel;
        private CustomProgressBar localProgressBar;
        private CustomProgressBar globalProgressBar;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button showDictionaryButton;
    }
}

