﻿// Copyright (c) 2015, 2016 Massimiliano Luzi, Mario Antonelli and Antonello Rizzi
// University of Rome "La Sapienza"
//
// massimiliano.luzi@uniroma1.it
// mario.antonelli@gmail.com 
// antonello.rizzi@uniroma1.it 
//
// This file is part of NotesPHInder.
//
// NotesPHInder is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// NotesPHInder is distributed in the hope that it will be useful. 
// IT IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
// PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
// CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with NotesPHInder.If not, see<http://www.gnu.org/licenses/>.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Windows.Forms;
using System.ComponentModel;

namespace NotesPHInder
{
    /// <summary>
    /// Class for wave file handling
    /// </summary>
    public class WaveHandler
    {
        #region private variable

        private byte[] waveFileBytes;       

        #endregion

        #region Private Properties

        /// <summary>
        /// Get the 'fmt ' chunck position
        /// </summary>
        private int FmtChunkPosition
        {
            get
            {
                int position = 0;

                while (!((char)(this.waveFileBytes[position]) == 'f' &&
                    (char)(this.waveFileBytes[position + 1]) == 'm' &&
                    (char)(this.waveFileBytes[position + 2]) == 't' &&
                    (char)(this.waveFileBytes[position + 3]) == ' '))
                {
                    position++;
                }
                return position;
            }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor: Read wave file bytes
        /// </summary>
        /// <param name="wavePath"></param>
        public WaveHandler (string wavePath)
        {
            waveFileBytes = File.ReadAllBytes(wavePath);
        }
        #endregion

        #region Public Properties

        /// <summary>
        /// Get audio format
        /// </summary>
        public short AudioFormat
        {
            get
            {
                int offset = FmtChunkPosition;
                return (short)(this.waveFileBytes[offset + 9] << 8 | this.waveFileBytes[offset + 8]);
            }
        }

        /// <summary>
        /// Get number of channel
        /// </summary>
        public int NumberOfChannel
        {
            get
            {
                int offset = FmtChunkPosition;
                return (int)(this.waveFileBytes[offset + 11] << 8 | this.waveFileBytes[offset + 10]);
            }
        }                

        /// <summary>
        /// Get Sample rate
        /// </summary>
        public int SampleRate
        {
            get 
            {
                int offset = FmtChunkPosition;
                return ( int )( this.waveFileBytes[offset + 15] << 24 | 
                    this.waveFileBytes[offset + 14] << 16 | 
                    this.waveFileBytes[offset + 13] << 8 | 
                    this.waveFileBytes[offset + 12] ); 
            }
        }
        
        /// <summary>
        /// Get Byte Rate
        /// </summary>
        public int ByteRate
        {
            get
            {
                int offset = FmtChunkPosition;
                return ( int )( this.waveFileBytes[offset + 19] << 24 | 
                    this.waveFileBytes[offset + 18] << 16 | 
                    this.waveFileBytes[offset + 17] << 8 | 
                    this.waveFileBytes[offset + 16] );
            }
        }

        /// <summary>
        /// Get all bytes
        /// </summary>
        public byte[] AllWaveBytes
        {
            get
            {
                return waveFileBytes;
            }
        }
                                
        #endregion

        #region Public Methods

        /// <summary>
        /// Extract the 16 bit sample from the Wave File.
        /// </summary>
        /// <returns> short[] array of the 16 bit sample.</returns>
        public short[] ExtractMonoSample()
        {
            // TODO:  add Exception Handler
            int dataLenght;
            int position = 0;
            short[] extractedSample;
            
            // Trova il chunkID="data"
            while (!((char)(this.waveFileBytes[position]) == 'd' &&
                (char)(this.waveFileBytes[position + 1]) == 'a' &&
                (char)(this.waveFileBytes[position + 2]) == 't' &&
                (char)(this.waveFileBytes[position + 3]) == 'a'))
            {
                position++;
            }

            position = position + 4;
            
            dataLenght = (int)(this.waveFileBytes[position + 3] << 24 |
                this.waveFileBytes[position + 2] << 16 |
                this.waveFileBytes[position + 1] << 8 |
                this.waveFileBytes[position]);

            extractedSample = new short[dataLenght / 2];
            
            position = position + 4;

            for (int i = 0; i < dataLenght / 2; i++)
            {
                extractedSample[i] = (short)(this.waveFileBytes[position + 1] << 8 | this.waveFileBytes[position]);
                position = position + 2;
            }
            
            return extractedSample;
        }

        /// <summary>
        /// Extract the 16 bit sample from the Wave File.
        /// </summary>
        /// <returns>
        /// Return two array of short[] corrispondig to left channel sample and right channel sample.
        /// </returns>
        public void ExtractStereoSample(out short[] leftSample, out short[] rightSample)
        {
            int dataLenght;
            int position = 44; // Start from the end of "fmt" chunk
            
            // Trova il chunkID="data"
            while (!((char)(this.waveFileBytes[position]) == 'd' &&
                (char)(this.waveFileBytes[position + 1]) == 'a' &&
                (char)(this.waveFileBytes[position + 2]) == 't' &&
                (char)(this.waveFileBytes[position + 3]) == 'a'))
            {
                position++;
            }

            position = position + 4;

            dataLenght = (int)(this.waveFileBytes[position + 3] << 24 |
                this.waveFileBytes[position + 2] << 16 |
                this.waveFileBytes[position + 1] << 8 |
                this.waveFileBytes[position]);

            leftSample = new short[dataLenght / 2];
            rightSample = new short[dataLenght / 2];

            position = position + 4;

            for (int i = 0; i < dataLenght / 4; i++)
            {
                leftSample[i] = (short)(this.waveFileBytes[position + 1] << 8 | this.waveFileBytes[position]);
                position = position + 2;

                rightSample[i] = (short)(this.waveFileBytes[position + 1] << 8 | this.waveFileBytes[position]);
                position = position + 2;
            }
        }

        /// <summary>
        /// Create an array of normalized double[] in -1 to +1 form an array of short[].
        /// </summary>
        /// <param name="samples">samples in 16 bit format</param>
        /// <returns>Samples as double[] type</returns>
        public double[] GetDoubleSamples(short[] samples)
        {
            double[] normalizedSamples = new double[samples.Length];

            int index = 0; 
            foreach ( short sample in samples )
            {
                normalizedSamples[index] = ((double)sample /32760);
                index++;
            }

            return normalizedSamples;
        }
        
        /// <summary>
        /// Apply linear normalization to samples in 16 bit format
        /// </summary>
        /// <param name="samples">samples in 16 bit format</param>
        /// <returns>Lienear normalized samples</returns>
        public double[] GetLinearNormalizedSamples(short[] samples)
        {
            double[] normalizedSamples = new double[samples.Length];

            short maxValue = short.MinValue;

            for (int i = 0; i < samples.Length; i++)
            {
                short value = Math.Abs(samples[i]);
                if (value > maxValue)
                {
                    maxValue = value;
                }
            }

            for (int i = 0; i < samples.Length; i++)
            {
                normalizedSamples[i] = (double)samples[i] / maxValue;
            }

            return normalizedSamples;
        }

        /// <summary>
        /// Apply linear normalization to samples just in double format.
        /// </summary>
        /// <param name="samples">samples in double format</param>
        /// <returns>Linear normalized samples</returns>
        public double[] GetLinearNormalizedSamples(double[] samples)
        {
            double[] normalizedSamples = new double[samples.Length];

            double maxValue = double.NegativeInfinity;

            for (int i = 0; i < samples.Length; i++)
            {
                double value = Math.Abs(samples[i]);
                if (value > maxValue)
                {
                    maxValue = value;
                }
            }

            for (int i = 0; i < samples.Length; i++)
            {
                normalizedSamples[i] = samples[i] / maxValue;
            }

            return normalizedSamples;
        }
        
        /// <summary>
        /// Create a wave file MONO PCM from shor[] array of sample.
        /// </summary>
        /// <param name="data">Sample of wave file.</param>
        /// <param name="sampleRate">Sampling frequency.</param>
        /// <param name="bitPerSample">Number of bit per Sample: 8, 16, 32</param>
        public void MakeWaveMonoPCM (short[] data, int sampleRate, short bitPerSample, string wavePath)
        {
            
            // RIFF chunk
            string riffChunkId = "RIFF";
            // TODO change data.Lenght*2 to allineate with different bitPerSample.
            int riffChunkDataSide = (data.Length*2) + 8 + 8 + 16; // data Lenght + 8WAVE + fmt chunk dimension
            string riffType = "WAVE";
                
            // fmt chunk
            string fmtChunkId = "fmt ";
            int fmtSize = bitPerSample;
            short fmtCompression = 1;
            short fmtNumberChannel = 1;
            int fmtSampleRate = sampleRate;
            int fmtAverageByteSecond = sampleRate*2;
            short fmtBlockAlign = 2;
            short fmtBitperSample = bitPerSample;
                
            // data chunk
            string dataChunkId = "data"; 
            int dataSize = data.Length*2;
            short[] dataSample = data;

            if (wavePath != null)
            {
                FileStream fw = new FileStream(wavePath, FileMode.Create);
                using (BinaryWriter bw = new BinaryWriter(fw))
                {
                    bw.Write(riffChunkId.ToCharArray());
                    bw.Write(riffChunkDataSide);
                    bw.Write(riffType.ToCharArray());

                    bw.Write(fmtChunkId.ToCharArray());
                    bw.Write(fmtSize);
                    bw.Write(fmtCompression);
                    bw.Write(fmtNumberChannel);
                    bw.Write(fmtSampleRate);
                    bw.Write(fmtAverageByteSecond);
                    bw.Write(fmtBlockAlign);
                    bw.Write(fmtBitperSample);

                    bw.Write(dataChunkId.ToCharArray());
                    bw.Write(dataSize);
                    for (int i = 0; i < dataSample.Length; i++)
                    {
                        bw.Write(dataSample[i]);
                    }
                }
            }
            }

        /// <summary>
        /// Create a wave file MONO PCM from double[] array of sample.
        /// </summary>
        /// <param name="samples">Samples to write</param>
        /// <param name="sampleRate">Sample rate</param>
        /// <param name="bitPerSample">number of bits per sample</param>
        /// <param name="wavePath">out path of wave file</param>
        public void MakeWaveMonoPCM(double[] samples, int sampleRate, short bitPerSample, string wavePath)
        {
            

            short[] data = new short[samples.Length];
            for (int i = 0; i < samples.Length; i++)
            {
                data[i] = (short)(samples[i] * 32760);
            }

            // RIFF chunk
            string riffChunkId = "RIFF";
            // TODO change data.Lenght*2 to allineate with different bitPerSample.
            int riffChunkDataSide = (data.Length * 2) + 8 + 8 + 16; // data Lenght + 8WAVE + fmt chunk dimension
            string riffType = "WAVE";

            // fmt chunk
            string fmtChunkId = "fmt ";
            int fmtSize = bitPerSample;
            short fmtCompression = 1;
            short fmtNumberChannel = 1;
            int fmtSampleRate = sampleRate;
            int fmtAverageByteSecond = sampleRate * 2;
            short fmtBlockAlign = 2;
            short fmtBitperSample = bitPerSample;

            // data chunk
            string dataChunkId = "data";
            int dataSize = data.Length * 2;
            short[] dataSample = data;

            if (wavePath != null)
            {
                FileStream fw = new FileStream(wavePath, FileMode.Create);
                using (BinaryWriter bw = new BinaryWriter(fw))
                {
                    bw.Write(riffChunkId.ToCharArray());
                    bw.Write(riffChunkDataSide);
                    bw.Write(riffType.ToCharArray());

                    bw.Write(fmtChunkId.ToCharArray());
                    bw.Write(fmtSize);
                    bw.Write(fmtCompression);
                    bw.Write(fmtNumberChannel);
                    bw.Write(fmtSampleRate);
                    bw.Write(fmtAverageByteSecond);
                    bw.Write(fmtBlockAlign);
                    bw.Write(fmtBitperSample);

                    bw.Write(dataChunkId.ToCharArray());
                    bw.Write(dataSize);
                    for (int i = 0; i < dataSample.Length; i++)
                    {
                        bw.Write(dataSample[i]);
                    }
                }
            }
        }

        /// <summary>
        /// Apply dynamic compression to samples
        /// </summary>
        /// <param name="samples">Samples in double format</param>
        /// <param name="threshold_dB">Threshold in dB</param>
        /// <param name="ratio">Ratio of compression</param>
        /// <param name="attackTime_ms">Attack time in ms</param>
        /// <param name="releaseTime_ms">Release time in ms</param>
        /// <param name="windowLength_ms">Window lenght for RMS computation in ms</param>
        /// <param name="sampleRate">Sample rate of wave file</param>
        /// <param name="worker">Background Worker</param>
        /// <returns>Compressed samples</returns>
        public double[] GetCompressionFilter(double[] samples, double threshold_dB, double ratio, double attackTime_ms, double releaseTime_ms, int windowLength_ms, int sampleRate, BackgroundWorker worker)
        {
            int perc = 0;

            // set windowLength
            int windowLength = (int)Math.Round(sampleRate * (double)windowLength_ms / 1000);
            // set increase and decrease rate
            double increaseCoefficent_dB = 10 / ((releaseTime_ms / 1000) * sampleRate);
            double decreaseCoefficent_dB = 10 / ((attackTime_ms / 1000) * sampleRate); 
            // inizialize gain
            double gain = 1.0;
            double gain_dB = 10*Math.Log10(gain);
            
            
            // inizialize out Samples
            double[] outSamples = samples;

            // Scan samples with windows of length windowLength

            for (int i = windowLength; i < samples.Length; i++)
            {
                if (worker != null && worker.CancellationPending)
                {
                    break;
                }
                else
                {
                    perc = (int)Math.Floor((100.0 * i) / (samples.Length));
                    worker.ReportProgress(perc);

                    // Inizialize List to extract the Peak Value
                    List<double> windowSamples = new List<double>();

                    // set range
                    int range = Math.Min(windowLength, samples.Length - i);

                    // compute rms value in window
                    double rmsValue_dB = 0.0;
                    for (int j = i - range; j < i; j++)
                    {
                        rmsValue_dB += (1.0 / range) * Math.Pow(samples[j], 2);
                        windowSamples.Add(Math.Abs(samples[j]));
                    }
                    rmsValue_dB = 10 * Math.Log10(Math.Sqrt(rmsValue_dB));

                    double peakValue = windowSamples.Max();
                    double maxGain = 1.0 / peakValue;
                    double minGain_dB = ((1 - ratio) / ratio) * (rmsValue_dB - threshold_dB);
                    windowSamples.Clear();

                    // If peak Value major than up threshold decrease gain
                    if (rmsValue_dB > threshold_dB)
                    {
                        gain_dB -= decreaseCoefficent_dB;
                        gain_dB = Math.Max(gain_dB, minGain_dB);
                    }
                    else
                    {
                        gain_dB += increaseCoefficent_dB;
                        gain_dB = Math.Min(0, gain_dB);
                    }

                    gain = Math.Min(Math.Pow(10, gain_dB / 10), maxGain);

                    outSamples[i] = gain * samples[i];
                }
            }
            
            return outSamples;
        }

        /// <summary>
        /// Compute the RMS value on all samples
        /// </summary>
        /// <param name="samples">Samples in double format</param>
        /// <returns>RMS value</returns>
        public double GetRsmDbValue(double[] samples)
        {
            double rms_dB = 0.0;

            for (int i = 0; i < samples.Length; i++)
            {
                rms_dB += (1.0 / samples.Length) * Math.Pow(samples[i], 2);
            }

            rms_dB = 10 * Math.Log10(rms_dB);
            return rms_dB;
        }

        /// <summary>
        /// Get envelope of samples
        /// </summary>
        /// <param name="data">samples</param>
        /// <param name="windowLength">window length for envelope estimation</param>
        /// <returns></returns>
        public double[] GetEnvelope(double[] data, int windowLength)
        {
            double[] envelopedSamples = new double[data.Length];

            List<double> samplesList = new List<double>();

            // Inizialize list
            for (int i = 0; i < windowLength / 2; i++)
            {
                samplesList.Add(0.0);
            }

            for (int i = 0; i < data.Length; i++)
            {
                samplesList.Add(Math.Abs(data[i]));
            }

            for (int i = data.Length + (windowLength / 2); i < data.Length; i++)
            {
                samplesList.Add(0.0);
            }

            for (int n = 0; n < data.Length - (windowLength / 2); n++)
            {
                List<double> windowedSamples = samplesList.GetRange(n, windowLength);
                envelopedSamples[n + (windowLength / 2)] = windowedSamples.Sum() / windowLength;
            }

            // Normalize envelope
            double maxValue = double.NegativeInfinity;
            for (int i = 0; i < envelopedSamples.Length; i++)
            {
                if (envelopedSamples[i] > maxValue)
                {
                    maxValue = envelopedSamples[i];
                }
            }

            for (int i = 0; i < envelopedSamples.Length; i++)
            {
                envelopedSamples[i] = envelopedSamples[i] / maxValue;
            }

            return envelopedSamples;
        }

        #endregion
    }
}
